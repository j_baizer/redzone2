-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: redzone
-- ------------------------------------------------------
-- Server version	5.7.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OwnerID` int(10) unsigned DEFAULT NULL COMMENT 'Who controls the asset',
  `UserID` int(10) unsigned DEFAULT NULL COMMENT 'Who has read-only access to the asset',
  `ParentID` int(10) unsigned DEFAULT NULL COMMENT 'Id of asset originally shared',
  `Payload` mediumtext COMMENT 'Up to 65k of encrypted payload',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  KEY `OwnerID` (`OwnerID`),
  KEY `UserID` (`UserID`),
  KEY `ParentID` (`ParentID`),
  CONSTRAINT `assets_ibfk_1` FOREIGN KEY (`OwnerID`) REFERENCES `users` (`Id`),
  CONSTRAINT `assets_ibfk_2` FOREIGN KEY (`UserID`) REFERENCES `users` (`Id`),
  CONSTRAINT `assets_ibfk_3` FOREIGN KEY (`ParentID`) REFERENCES `assets` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'timestamp of event',
  `sessionid` varchar(36) DEFAULT NULL COMMENT 'SessionID of connection-set by server',
  `requestid` varchar(36) DEFAULT NULL COMMENT 'RequestId of job-set by client',
  `entry` mediumtext COMMENT 'Stored as BLOBs with 64k max len',
  `level` int(11) DEFAULT '1' COMMENT 'Defined in com.redzone.server.Logger class',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Server-side event log';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL COMMENT 'Short name of role',
  `Description` varchar(45) DEFAULT NULL COMMENT 'Text description of role',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'user','Regular User in the System'),(2,'admin','Admin User in the System');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL COMMENT 'Login name of user',
  `Password` varchar(350) DEFAULT NULL COMMENT 'Authentication token',
  `PublicKey` varchar(255) DEFAULT NULL COMMENT 'Public Key of user-set at enrollment and password reset',
  `PrivateKeyE` mediumtext COMMENT 'Encrypted Private Key of User - set at enrollment or password reset',
  `IV` varchar(45) DEFAULT NULL,
  `Salt` varchar(45) DEFAULT NULL,
  `Active` tinyint(4) DEFAULT '0' COMMENT 'Boolean to enable or disable user account',
  `RoleID` int(11) NOT NULL DEFAULT '0' COMMENT 'Id of system role for this user',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`),
  UNIQUE KEY `Name_UNIQUE` (`Name`),
  KEY `users_ibfk_1` (`RoleID`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `roles` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Test 1','Iuyj9IPiMMHVOguRyG6zAfI3hLC2n0ucONkWXUA8JZ+wg4QR6cjmWudJdn6432tqZSRRX88oVKKYzr4akHTrjz9fsI737Zp+mnrA8u6L3duxrvjRcD0gVSaji12GkZrq0zMIgVAyipgkXy1dHctqzwsYzXdbXPNRw4ByIxTLcvhbPqtRCAwpi55XRldO9WtEz9ocD0pJvfdYg7hLONFMZGXPQjgtPeNdooiueRLibewViyKJ3l2MYO0Ev7Vw0mNUgNPsp/Ovmu3+8ZTeqyCswOaIXyzS1NEdWyNuTKDamtZaMYuG5DaxvY9cIDyiOEksPtPauwmhyBmY4elBlYVWjg==','MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQD2rMoygwo51MT7ygL7w+niGaL4rYFZUS9rukNejIQLJQLZN17oY7zqHKhxoQyDHwQ7LcRNIv+iTjgYabgZ0BFBiBNBan7nZhqPTzmf9hKluvIXBFPxPY61HiixcC4LyC5bNY+Ai1fIdgEwfGpLFbjcahohXU3GMhim5926c+LgbQIDAQAB','ugy5M+5P2UAk07OeJ39coOT94N2rAcoMv59FZGXEsPVcQvYydxCmHNTZYbw69Lk0J9NEY3yI8qgsT+fzUDZRJzY3YJTJbBl9gUEvVbXuay3fcXQQx/lupBrhHgVfMh+tCRmy2J9hdb+dyciae3TQNOmeYpj2NimimMNAzT83Msxbe8hE2vsZsAe/mv7RgvaKl3ZVFHGMpdKHjDTHP/h2Ifh1SONIyAW38izssw69wZIoP5S3e1tyWell+bXkDZzaHq+vbnYisUhUyy+ErRafB+RCkYYW5FQageJ/VWeagahmh0rjgdFawjEe9vKUIMAiuYfSqy7EQHHUmiVfPezk3t+vp04AdRq4YwTksH1fJCTzDqsshXUIB5lKgwu7Q1bELFEHNH1o5jKaYLQNHKtvTDUL/s8bzMQg9RK2gBo9jEp5BZ8GFHh7X8jOnI6BET0nlG48L30eDQQWDjPv301ZFuDrM0vYRZHluVQR7+66q/d6spdiIN4aLAv69yCLeQu6XIcyUDHGv5P4DtOwq2NBNfbkgo11hO3UhttflODbD27iX8mGu1sJAxjUi+gyiar2UBllax7tPJrOnXF4Iq6oQFn4OmIIf09MO+R6TVxWvyZOMmlX+L9JCCD6Kde9Qq9oab0CborSP+8B9BqMBulj/LWuCWjzkqfPx9od4TuQKbEV7t/7P6D4nhD1MapmDzmpxF/m0Fyz/yiF1JOk41siDcgRGKsb+5jFAGpmoD8G58ZXaxF8vXFzfDjABbRKiE1v3K0+rcTEGx1vYBWsZLSj93M8NF2vaAsj+JRnO4vzb8WNAH8D6Qgml79pOhsmpTmSEaJXKWtFRk2o22TjlKComUje3q8RaaD19nNa','TMwAVpP449TC3a+2','xXN7aS0Cw69rcoWU',1,0),(2,'Test 2','Iuyj9IPiMMHVOguRyG6zAfI3hLC2n0ucONkWXUA8JZ+wg4QR6cjmWudJdn6432tqZSRRX88oVKKYzr4akHTrjz9fsI737Zp+mnrA8u6L3duxrvjRcD0gVSaji12GkZrq0zMIgVAyipgkXy1dHctqzwsYzXdbXPNRw4ByIxTLcvhbPqtRCAwpi55XRldO9WtEz9ocD0pJvfdYg7hLONFMZGXPQjgtPeNdooiueRLibewViyKJ3l2MYO0Ev7Vw0mNUgNPsp/Ovmu3+8ZTeqyCswOaIXyzS1NEdWyNuTKDamtZaMYuG5DaxvY9cIDyiOEksPtPauwmhyBmY4elBlYVWjg==','MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCHNM0EXKN0nBPZWG1TJSdhg8VUI7YVRfx/LRaZZahNHtAwrzMeUF94KPpOqHH5cYrpEq6rSR6pAiz7/nRqZiD5KT4IB0i7w0Sjd1LE225p7rkzv2HwDM6MrCSfBne1kS7ZNJruIO3g3kmoMZHsE0vhc6ktdoIUnSkhrX4KxnK9YwIDAQAB','1GR0mwbeNkK2cMay1lf/KbqnllqWm8Brrlymbq5KLjeWyBuhVoi1Bs5+AdBKI/Kqs0hbkbWW7vg0X29mf42xPMESL2l4kz6sj0y8jIItt3gShMWoApmenS9uF05PAHy4NalUmR0yzfXsjahYmt1YHVPCmkA14ZNh4xy8RYJCQeU2988sg9tCaNvl2htn5C0+3o+XwSuri3yXEm8j4jIcufLkEcQudf5j+SclTb8al7Ujz56b3mLMAfEktt7yePDnHrasAZEOSOxOgOD7ZOKrtfbV20kCEBIGL5LeWXXhcwyXEZiNWSJgalLDnZolfHMQ5ZC9l+kvlcgYcuLnfh0CVD42KxqVXvUPKvj4OP2RaTPoQ3i5SmtamDgw2UtWLFE33wP6hM461Ru2jW/wBaDwffB1vqHVA78RJtTSaTGsY5snZr10v2gNhorgasx1HjPRuCIN7TspfrWq/iPcwLUB/89glvEVfOsNE5T3YSnvlzXYUZMYnEZ2fRANF59PAQES9AiRZmco3W3lymKwT3M3gSJgk4B+wl10grB6vLN3wOcYNZ5CCCjY2JnT46MKZfkOCuswjVgmpPIf8ffQEHvmFMmA61s0W0v+XILThTQOCbgA1nz38llWKoQtHIqNYZiBauHl0gOQ47QiDXI5D0DWsnu3X0hEYT4zEoebs+3gJn6ndi5U0enuuaqfip/6eZo8C5u+ByOtQGKg4gKJIyLGR8mr+QMfnTyh+L7PwV8x/b5PMBOM1EwmHgT5ysnY0xnJcCccY6n1Ay8pD3Jv1+uyRoVCXEKKlOxJ3Gj0auIvDI7vfjMgg0jhANZh6HUHP/IQri3OLduJjrzH6SOPl3OwXJLFj2++jlbeW4RFoQ==','ORfUpEOWlJ/t5mvD','kxqUP8IhhqTOb5Ax',1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-02 13:57:32
