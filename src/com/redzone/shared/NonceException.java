package com.redzone.shared;

public class NonceException extends RuntimeException {
	private static final long serialVersionUID = 471224912197872779L;

	public NonceException() {
	}

	public NonceException(String message) {
		super(message);
	}

	public NonceException(Throwable cause) {
		super(cause);
	}

	public NonceException(String message, Throwable cause) {
		super(message, cause);
	}

	public NonceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
