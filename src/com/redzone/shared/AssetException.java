package com.redzone.shared;

public class AssetException extends RuntimeException {
	private static final long serialVersionUID = 471224912197872779L;

	public AssetException() {
	}

	public AssetException(String message) {
		super(message);
	}

	public AssetException(Throwable cause) {
		super(cause);
	}

	public AssetException(String message, Throwable cause) {
		super(message, cause);
	}

	public AssetException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
