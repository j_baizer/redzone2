package com.redzone.shared;

import java.security.GeneralSecurityException;
import java.security.PublicKey;

import com.redzone.client.AsymmetricCrypto;

public class User {
	private int userid = 0;
	private String name = null;
	private PublicKey publicKey = null;
	private boolean valid = false;

	public User(String csv) throws InvalidArgumentException, UserAccountException {
		String[] parts = csv.split(":");
		if (parts.length != 3) {
			throw new InvalidArgumentException("Invalid number of fields (" + parts.length + ")!");
		}

		try {
			this.userid = Integer.parseInt(parts[0]);
			this.name = parts[1];
			this.setPublicKey(AsymmetricCrypto.base64ToPublicKey(parts[2]));
			this.valid = true;
		} catch (GeneralSecurityException e) {
			throw new UserAccountException(e);
		}
	}

	public int getUserid() {
		if (!isValid()) {
			return 0;
		}
		return userid;
	}

	public String getName() {
		if (!isValid()) {
			return null;
		}
		return name;
	}

	public PublicKey getPublicKey() {
		if (!isValid()) {
			return null;
		}
		return publicKey;
	}

	public boolean isValid() {
		return this.valid;
	}

	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}

	public String toString() {
		if (!isValid()) {
			return "INVALID";
		}
		return this.name;
	}

}
