package com.redzone.shared;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.redzone.server.Logger;

public class Packet {
	static final int MAX_PACKET_SIZE = 1000000;
	static final int TAG_NAME_LENGTH = 3;
	static final int MAX_ELEMENT_LENGTH = 10000;

	public static final int MIN_NAME_LENGTH = 3;
	public static final int MAX_NAME_LENGTH = 20;
	public static final int DIGEST_LENGTH = 44;

	private int packetVersion = 0;
	private String sessionID = "";
	private String requestID = "";
	private ArrayList<String> argv = new ArrayList<>();
	private String payload = "";
	private Logger log = null;
	private boolean valid = false;

	public Packet() {
		this.log = null;
	}

	public Packet(Logger log) {
		this.log = log;
	}

	public Packet(String xml) throws InvalidPayloadException, InvalidArgumentException {
		this(xml, null);
	}

	public Packet(String xml, Logger log) throws InvalidPayloadException, InvalidArgumentException {
		this.log = log;

		// ***SECURITY SENSITIVE: BE CAREFUL, AND THOROUGH!!
		// start with paranoid sanity checks...
		if (xml == null) {
			Exceptions.throwException(this.log, new InvalidPayloadException("Malformed (null) packet"), Logger.WARN);
		}
		if (xml.length() > MAX_PACKET_SIZE) {
			Exceptions.throwException(this.log, new InvalidPayloadException("Malformed (too large) packet"), Logger.WARN);
		}
		if (xml.trim().isEmpty()) {
			throw new InvalidArgumentException("Invalid XML");
		}

		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
			
			SAXParser saxParser = factory.newSAXParser();
			XMLHandler rzh = new XMLHandler();
			
			saxParser.parse(new InputSource(new StringReader(xml)), rzh);

			// if we are here, the XML passed all the tests and was read in
			this.packetVersion = rzh.getVer();
			this.sessionID = rzh.getSid();
			this.requestID = rzh.getRid();
			this.argv = rzh.getArgs();
			this.payload = rzh.getPay();
			this.valid = true;
		} catch (IOException | SAXException | ParserConfigurationException e) {
			Exceptions.throwException(this.log, new InvalidPayloadException("Malformed packet: " + xml), Logger.WARN);
		}

	}

	public String encodeToXML() {
		String retval = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		retval += "<rzp>";
		retval += "<ver>" + String.valueOf(this.packetVersion) + "</ver>";
		retval += "<sid>" + this.sessionID + "</sid>";
		retval += "<rid>" + this.requestID + "</rid>";

		if (this.argv != null) {
			retval += "<req>";
			for (Object arg : this.argv) {
				retval += "<arg>" + arg.toString() + "</arg>";
			}
			retval += "</req>";
		}

		retval += "<pay>" + this.payload + "</pay>";
		return retval + "</rzp>";

	}

	public String getSessionID() {
		return sessionID;
	}

	public void setRandomSessionID() {
		sessionID = UUID.randomUUID().toString();
	}

	public void setSessionID(String uuid) {
		sessionID = uuid;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID() {
		requestID = UUID.randomUUID().toString();
	}

	public void setRequestID(String id) {
		this.requestID = id;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public ArrayList<String> getArgv() {
		return argv;
	}

	public void setArgv(ArrayList<String> argv) {
		this.argv = argv;
	}

	public boolean isValid() {
		return this.valid;
	}
}
