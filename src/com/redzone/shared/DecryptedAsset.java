package com.redzone.shared;

import java.util.Arrays;

public class DecryptedAsset {
	private char[] name;
	private char[] value;

	public DecryptedAsset(char[] nullSeparated) throws InvalidArgumentException {
		int separatorPosition = -1;
		for (int i = 0; i < nullSeparated.length; i++) {
			if (nullSeparated[i] == (char) 0) {
				separatorPosition = i;
				break;
			}
		}

		if (separatorPosition < 0) {
			throw new InvalidArgumentException("No null byte in payload");
		}

		name = Arrays.copyOfRange(nullSeparated, 0, separatorPosition);
		value = Arrays.copyOfRange(nullSeparated, separatorPosition + 1, nullSeparated.length); // from java docs, "to - the final index of the range to be copied, exclusive. (This index may lie outside the array.)"
	}

	public DecryptedAsset(char[] name, char[] value) {
		this.name = name;
		this.value = value;
	}

	public char[] getName() {
		return name;
	}

	public void setName(char[] name) {
		this.name = name;
	}

	public char[] getValue() {
		return value;
	}

	public void setValue(char[] value) {
		this.value = value;
	}

	public void clean() {
		MiscUtils.clean(name);
		MiscUtils.clean(value);
	}

	public char[] getAsPlaintextPayload() {
		char result[] = new char[name.length + 1 + value.length];
		System.arraycopy(name, 0, result, 0, name.length);
		result[name.length] = (char) 0;
		System.arraycopy(value, 0, result, name.length + 1, value.length);
		return result;
	}
}
