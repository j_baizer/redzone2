package com.redzone.shared;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;

public class MiscUtils {
	private MiscUtils() {
	}

	public static void clean(char[] c) {
		Arrays.fill(c, '\u0000');
	}

	public static void clean(byte[] b) {
		Arrays.fill(b, (byte) 0);
	}

	/**
	 * THIS IS PROBABLY NOT WHAT YOU'RE LOOKING FOR, SEE inlineCharsToBytes
	 * Note that this method does not clean any memory, except for temp vars
	 * @deprecated
	 * @param chars an input string of chars
	 * @return the corresponding utf-8 byte[]
	 */
	public static byte[] charsToBytes(char[] chars) {
		CharBuffer charBuffer = CharBuffer.wrap(chars);
		ByteBuffer byteBuffer = Charset.forName("UTF-8").encode(charBuffer);
		byte[] bytes = Arrays.copyOfRange(byteBuffer.array(), byteBuffer.position(), byteBuffer.limit()); // this copies the buffer's backing array to an array that we control so that we can control when the memory is wiped
		clean(charBuffer.array()); // since .array() returns the array that backs the buffer, this will clean up memory used by the buffer
		clean(byteBuffer.array());
		return bytes;
	}

	/**
	 * THIS IS PROBABLY NOT WHAT YOU'RE LOOKING FOR, SEE inlineBytesToChars
	 * Note that this method does not clean any memory, except for temp vars
	 * @deprecated
	 * @param bytes an input string of bytes
	 * @return the corresponding char[] for the utf-8 input bytes
	 */
	public static char[] bytesToChars(byte[] bytes) {
		ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
		CharBuffer charBuffer = Charset.forName("UTF-8").decode(byteBuffer);
		char[] chars = Arrays.copyOfRange(charBuffer.array(), charBuffer.position(), charBuffer.limit()); // this copies the buffer's backing array to an array that we control so that we can control when the memory is wiped
		clean(charBuffer.array()); // since .array() returns the array that backs the buffer, this will clean up memory used by the buffer
		clean(byteBuffer.array());
		return chars;
	}

	/**
	 * Converts chars to bytes, cleaning the input char array before returning. Note that this will destroy any inputs passed to it.
	 * @param chars an input string of chars
	 * @return the corresponding utf-8 byte[]
	 */
	public static byte[] inlineCharsToBytes(char[] chars) {
		byte[] toReturn = charsToBytes(chars);
		clean(chars);
		return toReturn;
	}

	/**
	 * Converts bytes to chars, cleaning the input byte array before returning. Note that this will destroy any inputs passed to it.
	 * @param bytes an input string of bytes
	 * @return the corresponding char[] for the utf-8 input bytes
	 */
	public static char[] inlineBytesToChars(byte[] bytes) {
		char[] toReturn = bytesToChars(bytes);
		clean(bytes);
		return toReturn;
	}

	public static boolean contains(char[] haystack, char[] needle) {
		int n = haystack.length;
		int m = needle.length;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m && i + j < n; j++) {
				if (haystack[i + j] != needle[j]) {
					break;
				}
				if (j == m - 1) {
					return true;
				}
			}
		}
		return false;
	}
}
