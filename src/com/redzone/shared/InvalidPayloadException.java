package com.redzone.shared;

public class InvalidPayloadException extends RedzoneException {
	private static final long serialVersionUID = 471224912197872779L;

	public InvalidPayloadException() {
	}

	public InvalidPayloadException(String message) {
		super(message);
	}

	public InvalidPayloadException(Throwable cause) {
		super(cause);
	}

	public InvalidPayloadException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidPayloadException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
