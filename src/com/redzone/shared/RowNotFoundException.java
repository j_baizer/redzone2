package com.redzone.shared;

import java.io.IOException;

public class RowNotFoundException extends IOException {
	private static final long serialVersionUID = -3816788126272799762L;

	public RowNotFoundException() {
		super();
	}

	public RowNotFoundException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public RowNotFoundException(String arg0) {
		super(arg0);
	}

	public RowNotFoundException(Throwable arg0) {
		super(arg0);
	}

}
