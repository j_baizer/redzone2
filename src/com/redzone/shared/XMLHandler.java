package com.redzone.shared;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

class XMLHandler extends DefaultHandler {
	private String tag = "";
	private int ver = 0;
	private String sid = "";
	private String rid = "";
	private ArrayList<String> args = new ArrayList<>();
	private String pay = "";

	@Override
	public void startElement(String uri, String localName, String tagName, Attributes attributes) {
		if (tagName.length() != Packet.TAG_NAME_LENGTH) {
			throw new XMLException("Illegal XML tag name " + tagName);
		}

		tag = tagName;

	}

	@Override
	public void characters(char ch[], int start, int length) {
		if (length > Packet.MAX_ELEMENT_LENGTH) {
			throw new XMLException("XML element too long: " + length);
		} else if (length < 1) {
			throw new XMLException("Empty XML element");
		}

		String val = new String(ch, start, length);
		if (val.startsWith("\n")) {
			return;
		}

		switch (this.tag.substring(0, Packet.TAG_NAME_LENGTH)) {
			case "rzp": // these are valid tags, but ignored here
			case "req":
				break;
			case "ver":
				this.ver = Integer.parseInt(new String(ch, start, length));
				break;
			case "sid":
				this.sid = new String(ch, start, length);
				break;
			case "rid":
				this.rid = new String(ch, start, length);
				break;
			case "arg":
				this.args.add(new String(ch, start, length));
				break;
			case "pay":
				this.pay = new String(ch, start, length);
				break;
			default:
				throw new XMLException("Illegal tag: " + this.tag);

		}

	}

	int getVer() {
		return ver;
	}

	String getSid() {
		return sid;
	}

	String getRid() {
		return rid;
	}

	ArrayList<String> getArgs() {
		return args;
	}

	String getPay() {
		return pay;
	}

}