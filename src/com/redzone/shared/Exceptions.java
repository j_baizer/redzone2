package com.redzone.shared;

import com.redzone.server.Logger;

public class Exceptions {
	public static <T extends Throwable> void throwException(Logger logger, T exp, int level) throws T {
		if (logger == null) {
			exp.printStackTrace();
		} else {
			logger.write(exp.toString(), level);
		}
		
		throw exp;
	}
}
