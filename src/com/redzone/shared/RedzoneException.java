package com.redzone.shared;

public class RedzoneException extends Exception {
	private static final long serialVersionUID = 1279956160266161387L;

	public RedzoneException() {
		super();
	}

	public RedzoneException(String message) {
		super(message);
	}

	public RedzoneException(String message, Throwable cause) {
		super(message, cause);
	}

	public RedzoneException(Throwable cause) {
		super(cause);
	}

	protected RedzoneException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
