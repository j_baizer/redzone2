package com.redzone.shared;

import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.redzone.client.AsymmetricCrypto;
import com.redzone.client.ClientConnection;

public class Asset {
	private int assetId;
	private int ownerId;
	private int userId;
	private String payload;
	private boolean valid = false;
	private List<Integer> sharedWith = new ArrayList<>();

	public Asset(String arg) throws InvalidArgumentException {
		if (arg == null || arg.trim().isEmpty()) {
			throw new InvalidArgumentException("Invalid argument");
		}

		String[] parts = arg.split(":");
		if (parts.length != 5) {
			throw new InvalidArgumentException("Invalid number of arg fields (" + parts.length + ")");
		}

		try {
			this.assetId = Integer.parseInt(parts[0]);
			this.ownerId = Integer.parseInt(parts[1]);
			this.userId = Integer.parseInt(parts[2]);
			this.sharedWith = Arrays.asList(parts[3].split(",")).stream().filter(x -> !x.isEmpty()).mapToInt(Integer::parseInt).boxed().collect(Collectors.toList());
			this.payload = parts[4];
			this.valid = true;
		} catch (NumberFormatException e) {
			throw new InvalidArgumentException("Invalid ID number in arg fields", e);
		}

	}

	public Asset(int id) {
		this.assetId = id;
	}

	public int getAssetID() {
		return assetId;
	}

	public void setAssetID(int assetId) {
		this.assetId = assetId;
	}

	public int getOwnerID() {
		return ownerId;
	}

	public void setOwnerID(int ownerId) {
		this.ownerId = ownerId;
	}

	public int getUserID() {
		return userId;
	}

	public void setUserID(int userId) {
		this.userId = userId;
	}

	public String getPayload() {
		return payload;
	}

	public DecryptedAsset decryptPayload(ClientConnection rzcc, PrivateKey pk) throws ConnectionException, UserAccountException, InvalidPayloadException, InvalidArgumentException, CryptoException {
		try {
			char[] charPayload = MiscUtils.inlineBytesToChars(AsymmetricCrypto.decrypt(payload, pk, rzcc.userList().get(this.getOwnerID()).getPublicKey()));
			DecryptedAsset toReturn = new DecryptedAsset(charPayload);
			MiscUtils.clean(charPayload);
			return toReturn;
		} catch (IllegalArgumentException | GeneralSecurityException e) {
			throw new CryptoException("Decryption error in RZAsset.decryptPayload()", e);
		}
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public boolean isValid() {
		return this.valid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + assetId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Asset other = (Asset) obj;
		if (assetId != other.assetId) {
			return false;
		}
		return true;
	}

	public String toString() {
		String retval = this.assetId + ":" + this.ownerId + ":" + this.userId + ":" + this.payload;
		return retval;
	}

	public List<Integer> getSharedWith() {
		return sharedWith;
	}

	public void setSharedWith(List<Integer> sharedWith) {
		this.sharedWith = sharedWith;
	}
}
