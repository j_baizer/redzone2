package com.redzone.shared;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import com.redzone.client.ClientConfig;

public class HashUtils {
	private static final String HASH_ALGORITHM = "SHA-256";
	private static final int PASSWORD_ITER_COUNT = 12288;
	private static final int DERIVED_KEY_LENGTH = 256;
	private static final String KDF_ALGORITHM = "PBKDF2WithHmacSHA256";

	private HashUtils(){}
	
	private static final MessageDigest hash;
	private static final SecretKeyFactory kdf;
	
	static {
		try {
			hash = MessageDigest.getInstance(HASH_ALGORITHM);
			kdf = SecretKeyFactory.getInstance(KDF_ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static byte[] hash(byte[]... bytes){
		hash.reset();
		for (byte[] b : bytes){
			hash.update(b);
		}
		return hash.digest();
	}
	
	// based on https://www.veracode.com/blog/research/encryption-and-decryption-java-cryptography
	public static byte[] kdf(char[] pw) throws CryptoException {
		try {
			KeySpec spec = new PBEKeySpec(pw, ClientConfig.PASSWORD_HASH_SALT.getBytes(), PASSWORD_ITER_COUNT, DERIVED_KEY_LENGTH * 8);
			return kdf.generateSecret(spec).getEncoded();
		} catch (InvalidKeySpecException e) {
			throw new CryptoException(e);
		}
	}
	
}
