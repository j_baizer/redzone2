package com.redzone.client;

import com.redzone.shared.Packet;

public class Response {
	protected Packet rzp = null;

	public Response(Packet rzp) {
		this.rzp = rzp;
	}

	public String getSessionID() {
		return rzp.getSessionID();
	}

	public String getRequestID() {
		return rzp.getRequestID();
	}

	public String getPayload() {
		return rzp.getPayload();
	}
}
