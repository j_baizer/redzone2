package com.redzone.client;

import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Base64;

import javax.crypto.SecretKey;

import com.redzone.shared.Asset;
import com.redzone.shared.CryptoException;
import com.redzone.shared.InvalidArgumentException;
import com.redzone.shared.InvalidPayloadException;
import com.redzone.shared.Packet;

public class ResponseAuthentication extends Response {
	private ArrayList<Asset> assets = new ArrayList<>();
	private int userid = 0;
	private String epvtkey;
	private String iv;
	private String salt;
	private PublicKey publicKey;

	public ResponseAuthentication(Packet rzp)
			throws GeneralSecurityException, InvalidPayloadException, InvalidArgumentException {
		super(rzp);
		if (!rzp.isValid() || rzp.getArgv() == null || rzp.getArgv().size() < 4) {
			throw new InvalidPayloadException("Invalid packet");
		}

		this.userid = Integer.parseInt(rzp.getArgv().get(0));
		this.epvtkey = rzp.getArgv().get(1);
		this.iv = rzp.getArgv().get(2);
		this.salt = rzp.getArgv().get(3);
		this.publicKey = AsymmetricCrypto.base64ToPublicKey(rzp.getArgv().get(4));
		for (int i = 5; i < rzp.getArgv().size(); i++) {
			Asset asset = new Asset(rzp.getArgv().get(i));
			if (asset.isValid()) {
				this.assets.add(asset);
			}
		}
	}

	public PrivateKey getPrivateKey(char[] p) throws CryptoException {
		PrivateKey pk = null;
		try {
			SecretKey derived = SymmetricCrypto.getKey(p, Base64.getDecoder().decode(this.salt));
			pk = AsymmetricCrypto.bytesToPrivateKey(SymmetricCrypto.decrypt(Base64.getDecoder().decode(this.epvtkey), derived, this.iv));
		} catch (GeneralSecurityException e) {
			throw new CryptoException(e);
		}
		return pk;
	}

	public ArrayList<Asset> getAssets() {
		return this.assets;
	}

	public int getUserID() {
		return userid;
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}
}
