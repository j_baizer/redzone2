package com.redzone.client;

import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.redzone.shared.AssetException;
import com.redzone.shared.ConnectionException;
import com.redzone.shared.CryptoException;
import com.redzone.shared.MiscUtils;
import com.redzone.shared.Asset;
import com.redzone.shared.DecryptedAsset;
import com.redzone.shared.InvalidArgumentException;
import com.redzone.shared.InvalidPayloadException;
import com.redzone.shared.User;
import com.redzone.shared.UserAccountException;

public class AssetManager {
	private ClientConnection rzcc;
	private HashMap<Asset, DecryptedAsset> mapping = new HashMap<>();
	private PrivateKey privateKey;

	public AssetManager(ClientConnection rzcc) {
		this.rzcc = rzcc;
	}

	public void updateList()
			throws ConnectionException, InvalidPayloadException, UserAccountException, InvalidArgumentException, CryptoException {
		this.clean();
		Collection<Asset> assets = rzcc.assetList(true).values();

		if (assets == null) {
			throw new AssetException("An error occured while fetching the asset list");
		}

		for (Asset asset : assets) {
			mapping.put(asset, asset.decryptPayload(rzcc, privateKey));
		}
	}

	public int update(Asset asset) throws ConnectionException, InvalidPayloadException, InvalidArgumentException, CryptoException {
		byte[] bytePayload = MiscUtils.inlineCharsToBytes(mapping.get(asset).getAsPlaintextPayload());
		int toReturn = rzcc.update(asset.getAssetID(), bytePayload, privateKey);
		MiscUtils.clean(bytePayload);

		if (toReturn == 0) {
			throw new AssetException("An error occured while updating");
		}

		return toReturn;
	}

	public Asset add(DecryptedAsset decryptedAsset)
			throws ConnectionException, InvalidPayloadException, InvalidArgumentException {
		byte[] payload = MiscUtils.inlineCharsToBytes(decryptedAsset.getAsPlaintextPayload());
		int assetId = rzcc.add(payload, privateKey);
		MiscUtils.clean(payload);

		if (assetId == 0) {
			throw new AssetException("An error occured while adding");
		}

		Asset asset = new Asset(assetId);
		asset.setUserID(rzcc.getUserID());
		asset.setOwnerID(rzcc.getUserID());
		mapping.put(asset, decryptedAsset);
		return asset;
	}

	public void delete(Asset asset) throws ConnectionException, InvalidPayloadException, InvalidArgumentException {
		rzcc.delete(asset.getAssetID());
		mapping.remove(asset);
	}

	public DecryptedAsset getDecrypted(Asset asset) {
		return mapping.get(asset);
	}

	public int share(Asset asset, int otheruserid)
			throws ConnectionException, UserAccountException, InvalidPayloadException, InvalidArgumentException, CryptoException {
		int toReturn = rzcc.share(privateKey, asset.getAssetID(), otheruserid);

		if (toReturn == 0) {
			throw new AssetException("An error occured while sharing");
		}

		return toReturn;
	}

	public Collection<User> getUserList() throws ConnectionException, UserAccountException, InvalidPayloadException, InvalidArgumentException {
		return rzcc.userList().values();
	}

	public ArrayList<Asset> search(char[] substr) {
		ArrayList<Asset> toReturn = new ArrayList<>();
		for (Asset asset : mapping.keySet()) {
			if (MiscUtils.contains(mapping.get(asset).getName(), substr)) {
				toReturn.add(asset);
			}
		}
		return toReturn;
	}

	public ArrayList<Asset> getAll() {
		return new ArrayList<>(mapping.keySet());
	}

	public void clean() {
		Iterator<Asset> iterator = mapping.keySet().iterator();
		while (iterator.hasNext()) {
			Asset entry = iterator.next();
			if (mapping.get(entry) != null) {
				mapping.get(entry).clean();
			}
			iterator.remove();
		}
	}

	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}
}
