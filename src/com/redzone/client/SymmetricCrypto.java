package com.redzone.client;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.redzone.shared.CryptoException;

public class SymmetricCrypto {
	public static final int AES_KEY_SIZE = 128;
	public static final int NONCE_LENGTH = 12;
	public static final int TAG_LENGTH = 128;
	private static final String ENCRYPTION_ALGORITHM = "AES/GCM/NoPadding";

	private static final int ITERATION_COUNT = 65536;
	private static final int KEY_LENGTH = 128;
	private static final String KEY_ALGORITHM = "PBKDF2WithHmacSHA256";
	private static final String KEY_SPEC_ALGORITHM = "AES";

	private static final Cipher cipher;
	private static final SecretKeyFactory keyFactory;
	private static final KeyGenerator keyGen;

	static {
		try {
			cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM, "SunJCE");
			keyFactory = SecretKeyFactory.getInstance(KEY_ALGORITHM);
			keyGen = KeyGenerator.getInstance(KEY_SPEC_ALGORITHM);
			keyGen.init(KEY_LENGTH);
		} catch (GeneralSecurityException e) {
			throw new RuntimeException(new CryptoException(e));
		}
	}
	
	private SymmetricCrypto() {}

	public static byte[] encrypt(byte[] plaintext, SecretKey key, byte[] nonce) throws GeneralSecurityException {
		GCMParameterSpec params = new GCMParameterSpec(TAG_LENGTH, nonce);
		cipher.init(Cipher.ENCRYPT_MODE, key, params);

		return cipher.doFinal(plaintext);
	}

	public static byte[] decrypt(byte[] ciphertext, SecretKey key, byte[] nonce) throws GeneralSecurityException {
		GCMParameterSpec params = new GCMParameterSpec(TAG_LENGTH, nonce);
		cipher.init(Cipher.DECRYPT_MODE, key, params);

		return cipher.doFinal(ciphertext);
	}

	public static byte[] decrypt(byte[] ciphertext, SecretKey key, String base64nonce) throws GeneralSecurityException {
		return decrypt(ciphertext, key, Base64.getDecoder().decode(base64nonce));
	}

	public static byte[] randomNonce() throws NoSuchAlgorithmException {
		final byte[] nonce = new byte[NONCE_LENGTH];
		SecureRandom.getInstanceStrong().nextBytes(nonce);
		return nonce;
	}

	public static SecretKey getKey(char[] password, byte[] salt) throws InvalidKeySpecException {
		KeySpec keySpec = new PBEKeySpec(password, salt, ITERATION_COUNT, KEY_LENGTH);
		SecretKey secretKey = keyFactory.generateSecret(keySpec);

		return new SecretKeySpec(secretKey.getEncoded(), KEY_SPEC_ALGORITHM);
	}

	public static SecretKey randomKey() {
		return keyGen.generateKey();
	}

	public static SecretKey keyFromBytes(byte[] bytes) {
		return new SecretKeySpec(bytes, 0, bytes.length, KEY_SPEC_ALGORITHM);
	}

}
