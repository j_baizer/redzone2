package com.redzone.client;

import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import com.redzone.shared.CryptoException;

public class AsymmetricCrypto {
	private static final String ENCRYPTION_ALGORITHM = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding";
	private static final String SIGNATURE_ALGORITHM = "SHA256withRSA";

	private static final String KEY_TYPE = "RSA";
	private static final int KEY_SIZE = 1024;

	private static final Cipher cipher;
	private static final KeyPairGenerator keyGenerator;
	private static final Signature signatureGenerator;

	static {
		try {
			cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
			keyGenerator = KeyPairGenerator.getInstance(KEY_TYPE);
			keyGenerator.initialize(KEY_SIZE);
			signatureGenerator = Signature.getInstance(SIGNATURE_ALGORITHM);
		} catch (GeneralSecurityException e) {
			throw new RuntimeException(new CryptoException("Exception while initializing AsymmetricCrypto", e));
		}
	}

	private AsymmetricCrypto() {
	}

	/**
	 * Performs asymmetric authenticated encryption using a combination of RSA
	 * and AES-GCM. This allows for unlimited length (by using symmetric crypto)
	 * while still maintaining asymmetric features
	 * 
	 * @param plaintext
	 *            input plaintext to encrypt
	 * @param publicKey
	 *            the public key to encrypt using
	 * @param signerPrivateKey
	 *            the private key the data will be signed with
	 * @return a string of the format base64(RSA(AES key))),base64(AES
	 *         nonce),base64(AES-GCM(plaintext, AES key)),base64(RSASIG(first 3
	 *         elements, signerPrivateKey))
	 */
	public static String encrypt(byte[] plaintext, PublicKey publicKey, PrivateKey signerPrivateKey)
			throws GeneralSecurityException {
		String toReturn = "";

		cipher.init(Cipher.ENCRYPT_MODE, publicKey);

		SecretKey tempKey = SymmetricCrypto.randomKey();
		byte[] tmpNonce = SymmetricCrypto.randomNonce();

		toReturn += Base64.getEncoder().encodeToString(cipher.doFinal(tempKey.getEncoded())); // append RSA(AES key)
		toReturn += ",";
		toReturn += Base64.getEncoder().encodeToString(tmpNonce); // append nonce to be used during AES
		toReturn += ",";
		toReturn += Base64.getEncoder().encodeToString(SymmetricCrypto.encrypt(plaintext, tempKey, tmpNonce)); // append AES(plaintext)
		toReturn += ",";

		signatureGenerator.initSign(signerPrivateKey);
		signatureGenerator.update(toReturn.getBytes());

		toReturn += Base64.getEncoder().encodeToString(signatureGenerator.sign()); // sign the whole thing

		return toReturn;
	}

	/**
	 * Performs an authenticated decryption on the provided input string.
	 * 
	 * @param b64ciphertext
	 *            the input to be decrypted. See the return section of the
	 *            encrypt method for details of the format.
	 * @param privateKey
	 *            the private key to be used during decryption
	 * @param signerPublicKey
	 *            the public key that was used to sign the data
	 * @return the decrypted plaintext
	 */
	public static byte[] decrypt(String b64ciphertext, PrivateKey privateKey, PublicKey signerPublicKey)
			throws GeneralSecurityException {
		String[] parts = b64ciphertext.split(",");
		if (parts.length != 4) {
			throw new SignatureException("Unable to split message in to 4 parts");
		}

		byte[] signedPortion = b64ciphertext.substring(0, b64ciphertext.lastIndexOf(",") + 1).getBytes(); // chop the signature off the message to get the signed portion

		byte[] encryptedSymmetricKey = Base64.getDecoder().decode(parts[0]);
		byte[] nonce = Base64.getDecoder().decode(parts[1]);
		byte[] ciphertext = Base64.getDecoder().decode(parts[2]);
		byte[] signature = Base64.getDecoder().decode(parts[3]);

		signatureGenerator.initVerify(signerPublicKey);
		signatureGenerator.update(signedPortion);

		if (!signatureGenerator.verify(signature)) {
			throw new SignatureException("Signature verification failed.");
		}

		cipher.init(Cipher.DECRYPT_MODE, privateKey);

		return SymmetricCrypto.decrypt(ciphertext, SymmetricCrypto.keyFromBytes(cipher.doFinal(encryptedSymmetricKey)), nonce);
	}

	public static KeyPair randomKey() {
		return keyGenerator.generateKeyPair();
	}

	public static String keyToBase64(Key key) {
		return Base64.getEncoder().encodeToString(key.getEncoded());
	}

	public static PublicKey base64ToPublicKey(String base64) throws GeneralSecurityException {
		return KeyFactory.getInstance(KEY_TYPE).generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(base64)));
	}

	public static PrivateKey bytesToPrivateKey(byte[] bytes) throws GeneralSecurityException {
		return KeyFactory.getInstance(KEY_TYPE).generatePrivate(new PKCS8EncodedKeySpec(bytes));
	}

	public static PrivateKey base64ToPrivateKey(String base64) throws GeneralSecurityException {
		return bytesToPrivateKey(Base64.getDecoder().decode(base64));
	}
}
