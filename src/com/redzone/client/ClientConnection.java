package com.redzone.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;

import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import com.redzone.shared.Asset;
import com.redzone.shared.AssetException;
import com.redzone.shared.AuthenticationException;
import com.redzone.shared.ConnectionException;
import com.redzone.shared.CryptoException;
import com.redzone.shared.DecryptedAsset;
import com.redzone.shared.HashUtils;
import com.redzone.shared.InvalidArgumentException;
import com.redzone.shared.InvalidPayloadException;
import com.redzone.shared.MiscUtils;
import com.redzone.shared.Packet;
import com.redzone.shared.User;
import com.redzone.shared.UserAccountException;

public class ClientConnection {
	private String serverAddress;
	private int serverPort;
	private PrintWriter out;
	private BufferedReader in;
	private String sessionID = "";
	private HashMap<Integer, Asset> assets;
	private HashMap<Integer, User> users;
	private ResponseAuthentication auth;
	private SSLSocket socket;

	public ClientConnection(String addr, int port) throws ConnectionException {
		this.serverAddress = addr;
		this.serverPort = port;
		this.connect();
	}

	public void authenticate(String user, char[] pass) throws ConnectionException, InvalidPayloadException, InvalidArgumentException, CryptoException {
		// start by running our authentication password through a KDF
		byte[] kdf = HashUtils.kdf(pass);

		// okay, kdf is what is stored in the DB on the server, but... its not safe to send cleartext, so get a nonce from the server...
		Packet request = new Packet();
		ArrayList<String> args = new ArrayList<>();
		args.add("init");
		request.setArgv(args);
		this.send(request);

		Packet reply = this.recv();
		if (!reply.isValid()) {
			throw new ConnectionException("Connection dropped");
		}

		System.out.println("NONCE:  " + reply.getPayload());

		byte[] hash = HashUtils.hash(Base64.getEncoder().encode(kdf), reply.getPayload().getBytes());

		// now send the authentication request back to server...
		request = new Packet();
		args = new ArrayList<>();
		args.add("auth");
		args.add(user);
		args.add(Base64.getEncoder().encodeToString(hash));
		request.setArgv(args);
		request.setSessionID(reply.getSessionID()); // carry this forward
		request.setRequestID();
		this.send(request);

		reply = this.recv();
		if (!reply.isValid()) {
			throw new ConnectionException("Connection dropped");
		}

		if (!reply.getRequestID().equals(request.getRequestID())) {
			throw new AuthenticationException("RequestID mismatch");
		}
		this.sessionID = reply.getSessionID();

		try {
			this.auth = new ResponseAuthentication(reply);
			this.assets = new HashMap<>();
			for (Asset rza : this.auth.getAssets()) {
				this.assets.put(rza.getAssetID(), rza);
			}
		} catch (GeneralSecurityException e) {
			throw new AuthenticationException(e);
		}

	}

	public int add(byte[] pt, PrivateKey pk) throws ConnectionException, InvalidPayloadException, InvalidArgumentException { // used for new assets
		if (this.auth == null) {
			throw new AuthenticationException("Cannot add without authentication");
		}

		// handle the encryption up front-unfortunately the lib throws exceptions...
		String ct;
		try {
			ct = AsymmetricCrypto.encrypt(pt, this.auth.getPublicKey(), pk);
		} catch (GeneralSecurityException e) {
			throw new AuthenticationException(e);
		}

		Packet request = new Packet();
		request.setSessionID(this.sessionID);
		request.setRequestID();

		ArrayList<String> args = new ArrayList<>();
		args.add("add");
		args.add(ct);
		request.setArgv(args);
		this.send(request);

		Packet reply = this.recv();
		if (!reply.isValid()) {
			throw new ConnectionException("Connection dropped");
		}
		if (!reply.getRequestID().equals(request.getRequestID())) {
			new AuthenticationException("RequestID mismatch");
		}

		int assetid = Integer.parseInt(reply.getPayload());
		if (assetid <= 0) {
			throw new InvalidPayloadException("Invalid response");
		}

		// try updating our cached asset list to include change
		this.assets = this.assetList(true);

		return assetid;

	}

	public int update(int assetid, byte[] pt, PrivateKey pk) throws ConnectionException, InvalidPayloadException, InvalidArgumentException, CryptoException {
		if (this.auth == null) {
			throw new AuthenticationException("Cannot update without authentication");
		}

		String ct;
		try {
			ct = AsymmetricCrypto.encrypt(pt, this.auth.getPublicKey(), pk);
		} catch (GeneralSecurityException e) {
			throw new AuthenticationException(e);
		}

		Packet request = new Packet();
		request.setSessionID(this.sessionID);
		request.setRequestID();

		ArrayList<String> args = new ArrayList<>();
		args.add("update");
		args.add(Integer.toString(assetid));
		args.add(ct);
		request.setArgv(args);
		this.send(request);

		Packet reply = this.recv();
		if (!reply.isValid()) {
			throw new ConnectionException("Connection dropped");
		}
		if (!reply.getRequestID().equals(request.getRequestID())) {
			throw new AuthenticationException("RequestID mismatch");
		}

		int returnid;
		try {
			returnid = Integer.parseInt(reply.getPayload());
			if (returnid <= 0) {
				throw new InvalidPayloadException("Invalid response");
			}
		} catch (NumberFormatException e) {
			throw new InvalidPayloadException("Invalid response (invalid integer in payload)");
		}

		// try updating our cached asset list to include change
		this.assets = this.assetList(true);
		
		for (int userId : assets.get(assetid).getSharedWith()){
			try {
				share(pk, assetid, userId);
			} catch (UserAccountException e) {
				throw new AssetException("Failed to update child copeis of asset", e);
			}
		}

		return returnid;

	}

	public int share(PrivateKey pk, int assetid, int otheruserid) throws UserAccountException, ConnectionException, InvalidPayloadException, InvalidArgumentException, CryptoException {
		if (this.assets == null) {
			throw new AssetException("No asset list loaded");
		}
		if (this.auth == null) {
			throw new AuthenticationException("Cannot share without authentication");
		}
		if (!this.assets.containsKey(assetid)) {
			throw new AssetException("Bad asset ID: '" + assetid + "'");
		}
		Asset rza = this.assets.get(assetid);
		DecryptedAsset pt = rza.decryptPayload(this, pk);
		if (pt == null) {
			throw new CryptoException("Decryption exception for asset: '" + assetid + "'");
		}

		// now get the other user's public key...
		if (this.users == null) {
			throw new UserAccountException("No user list");
		}
		if (!this.users.containsKey(otheruserid)) {
			throw new UserAccountException("Bad user ID");
		}
		PublicKey opk = this.users.get(otheruserid).getPublicKey();

		String ct;
		try {
			ct = AsymmetricCrypto.encrypt(MiscUtils.inlineCharsToBytes(pt.getAsPlaintextPayload()), opk, pk);
		} catch (GeneralSecurityException e) {
			throw new CryptoException(e);
		}

		// send that encrypted payload with the appropriate ids
		Packet request = new Packet();
		request.setSessionID(this.sessionID);
		request.setRequestID();

		ArrayList<String> args = new ArrayList<>();
		args.add("share");
		args.add(Integer.toString(assetid));
		args.add(Integer.toString(otheruserid));
		args.add(ct);
		request.setArgv(args);
		this.send(request);

		Packet reply = this.recv();
		if (!reply.isValid()) {
			throw new ConnectionException("Connection dropped");
		}
		if (!reply.getRequestID().equals(request.getRequestID())) {
			throw new AuthenticationException("RequestID mismatch");
		}

		int returnid = Integer.parseInt(reply.getPayload());
		if (returnid <= 0) {
			throw new InvalidPayloadException("Invalid response");
		}

		return returnid;

	}

	public void delete(int assetid) throws ConnectionException, InvalidPayloadException, InvalidArgumentException {
		Packet request = new Packet();
		request.setSessionID(this.sessionID);
		request.setRequestID();
		ArrayList<String> args = new ArrayList<>();
		args.add("delete");
		args.add(Integer.toString(assetid));
		request.setArgv(args);
		this.send(request);

		Packet reply = this.recv();
		if (!reply.isValid()) {
			throw new ConnectionException("Connection dropped");
		}
		if (!reply.getRequestID().equals(request.getRequestID())) {
			throw new AuthenticationException("RequestID mismatch");
		}

		int retid = Integer.parseInt(reply.getPayload());
		if (retid <= 0) {
			throw new InvalidPayloadException("Invalid response");
		}

		// try updating our cached asset list to include the change
		this.assets = this.assetList(true);
	}

	public HashMap<Integer, Asset> assetList() throws ConnectionException, InvalidPayloadException, InvalidArgumentException {
		return this.assetList(false);
	}

	public HashMap<Integer, Asset> assetList(boolean refresh) throws ConnectionException, InvalidPayloadException, InvalidArgumentException {
		if (!refresh && this.assets != null) {
			return this.assets;
		}

		Packet request = new Packet();
		request.setSessionID(this.sessionID);
		request.setRequestID();
		ArrayList<String> args = new ArrayList<>();
		args.add("alist");
		request.setArgv(args);
		this.send(request);

		Packet reply = this.recv();
		if (!reply.isValid()) {
			throw new ConnectionException("Connection dropped");
		}
		if (!reply.getRequestID().equals(request.getRequestID())) {
			throw new AuthenticationException("RequestID mismatch");
		}

		// they come back as CSVs, lets make them more user-friendly
		ArrayList<String> csvs = reply.getArgv();
		this.assets = new HashMap<>();
		for (int i = 0; i < csvs.size(); i++) {
			Asset rza = new Asset(csvs.get(i));
			if (rza.isValid()) {
				this.assets.put(rza.getAssetID(), rza);
			} else {
				throw new AuthenticationException("Unable to parse Asset CSV '" + csvs.get(i) + "'");
			}
		}

		return this.assets; // we hang on to a copy!
	}

	public HashMap<Integer, User> userList() throws ConnectionException, UserAccountException, InvalidPayloadException, InvalidArgumentException {
		return userList(false);
	}

	public HashMap<Integer, User> userList(boolean refresh) throws ConnectionException, UserAccountException, InvalidPayloadException, InvalidArgumentException {
		if (!refresh && this.users != null) {
			return this.users;
		}

		Packet request = new Packet();
		request.setSessionID(this.sessionID);
		request.setRequestID();
		ArrayList<String> args = new ArrayList<>();
		args.add("ulist");
		request.setArgv(args);
		this.send(request);

		Packet reply = this.recv();
		if (!reply.isValid()) {
			throw new ConnectionException("Connection dropped");
		}
		if (!reply.getRequestID().equals(request.getRequestID())) {
			throw new AuthenticationException("RequestID mismatch");
		}

		// they come back as CSVs, lets make them more user-friendly
		ArrayList<String> ul = reply.getArgv();
		this.users = new HashMap<>();
		for (int i = 0; i < ul.size(); i++) {
			User rzu = new User(ul.get(i));
			if (rzu.isValid()) {
				this.users.put(rzu.getUserid(), rzu);
			} else {
				throw new UserAccountException("Unable to parse User CSV '" + ul.get(i) + "'");
			}
		}
		return this.users;
	}

	public PrivateKey getPVK(char[] pw) throws CryptoException {
		return this.auth.getPrivateKey(pw);
	}

	public int getUserID() {
		if (this.auth == null) {
			throw new AuthenticationException("User authentication is not valid");
		}
		return this.auth.getUserID();
	}

	private void connect() throws ConnectionException {
		if (this.socket == null || !this.socket.isConnected()) {
			try {
				System.setProperty("javax.net.ssl.trustStore", ClientConfig.KEYSTORE_PATH);
				System.setProperty("javax.net.ssl.trustStorePassword", ClientConfig.KEYSTORE_PASSWORD);
				SSLSocketFactory ssf = (SSLSocketFactory) SSLSocketFactory.getDefault();
				socket = (SSLSocket) ssf.createSocket(this.serverAddress, this.serverPort);

				SSLSession session = socket.getSession();
				Certificate[] cchain = session.getPeerCertificates();
				System.out.println("The Certificates used by peer");
				for (Certificate celem : cchain) {
					System.out.println(((X509Certificate) celem).getSubjectDN());
				}
				System.out.println("Peer host is " + session.getPeerHost());
				System.out.println("Cipher is " + session.getCipherSuite());
				System.out.println("Protocol is " + session.getProtocol());
				
				this.out = new PrintWriter(socket.getOutputStream(), true);
				this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			} catch (IOException e) {
				throw new ConnectionException("Unable to establish connection", e);
			}
		}
	}

	private void send(Packet rzp) throws ConnectionException {
		if (this.socket.isOutputShutdown()) {
			throw new ConnectionException("Output is shutdown");
		}

		this.out.println(rzp.encodeToXML());
	}

	private Packet recv() throws ConnectionException, InvalidPayloadException, InvalidArgumentException {
		if (this.socket.isInputShutdown()) {
			throw new ConnectionException("Input is shutdown");
		}
		
		try {
			String response = this.in.readLine();
			if (response == null){
				throw new ConnectionException("Connection is closed");
			}
			return new Packet(response);
		} catch (IOException e) {
			throw new InvalidPayloadException("Exception while reading from scanner", e);
		}
		
	}
}
