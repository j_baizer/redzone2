package com.redzone.client;

public final class ClientConfig {
	public static final String KEYSTORE_PATH = "ssl/clientstore.jks";
	public static final String KEYSTORE_PASSWORD = "pass4redzone";
	public static final String SERVER_ADDRESS = "localhost";
	public static final int SERVER_PORT = 3411;
	public static final String PASSWORD_HASH_SALT = "RedZonePasswordSalt";
	
	private ClientConfig() {
	}
}
