package com.redzone.server;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;

import com.redzone.shared.DatabaseException;
import com.redzone.shared.HashUtils;
import com.redzone.shared.RowNotFoundException;

public class Database {
	private Connection dbconn;
	public Logger log;

	public Database(String connstr) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.dbconn = DriverManager.getConnection(Server.DB_CONNECTION_URL);
		} catch (ClassNotFoundException e) {
			throw new DatabaseException("JDBC Driver Not Found", e);
		} catch (SQLException e) {
			throw new DatabaseException(e);
		}

		this.log = new Logger(Server.LOGLEVEL, this.dbconn);
	}

	public void closeConnection() {
		try {
			dbconn.close();
		} catch (SQLException e) {
			System.err.println("Non-fatal exception caught while closing JDBC connection");
			e.printStackTrace();
		}
	}

	public Connection getConnection() {
		return dbconn;
	}

	public Logger getLog() {
		return log;
	}

	public String getPassHash(String user, String nonce) throws SQLException, NoSuchAlgorithmException, RowNotFoundException {
		final String sql = "SELECT Password FROM Users WHERE Name = ?";
		PreparedStatement ps = this.dbconn.prepareStatement(sql);
		ps.setString(1, user);

		ResultSet rs = ps.executeQuery();
		if (!rs.first()) {
			throw new RowNotFoundException("getPassHash for user " + user + " returned no results");
		}
		byte[] buf = rs.getBytes("Password");
		
		byte[] hb = HashUtils.hash(buf, nonce.getBytes());

		return Base64.getEncoder().encodeToString(hb);
	}

	public ArrayList<String> getMetaData(String user) throws RowNotFoundException, SQLException {
		ArrayList<String> retval = new ArrayList<>();

		String sql = "SELECT Id, PrivateKeyE, IV, Salt, PublicKey FROM Users WHERE Name = ?";
		PreparedStatement ps = this.dbconn.prepareStatement(sql);
		ps.setString(1, user);
		
		ResultSet rs = ps.executeQuery();
		if (!rs.first()) {
			throw new RowNotFoundException("getMetaData for user " + user + " returned no results");
		}
		
		retval.add(rs.getString("Id"));
		retval.add(rs.getString("PrivateKeyE"));
		retval.add(rs.getString("IV"));
		retval.add(rs.getString("Salt"));
		retval.add(rs.getString("PublicKey"));

		// if we made it here, we should be all good
		return retval;

	}

	public ArrayList<String> getAssets(int uid) throws SQLException {
		ArrayList<String> retval = new ArrayList<>();
		// put list of user's assets in args array
		String sql = "SELECT am.*, au.sharedWith " +
					"FROM assets am " +
					"LEFT JOIN (SELECT ParentID, group_concat(UserID) as sharedWith FROM assets where UserId <> OwnerID) au on au.ParentID = am.ParentID " +
					"WHERE am.UserID = ?";
		PreparedStatement ps = this.dbconn.prepareStatement(sql);
		ps.setInt(1, uid);
		
		ResultSet rs = ps.executeQuery();
		
		if (!rs.first()) { // if user has no assets
			return retval;
		}
		
		do {
			String sharedWith = rs.getString("sharedWith");
			if (sharedWith == null){
				sharedWith = "";
			}
			retval.add(rs.getString("Id") + ":" + rs.getString("OwnerId") + ":" + rs.getString("UserId") + ":" + sharedWith + ":" + rs.getString("Payload"));
		} while (rs.next());
		
		return retval;
	}

	public ArrayList<String> getUsers() throws SQLException {
		ArrayList<String> retval = new ArrayList<>();
		// put list users in args array
		String sql = "SELECT Id, Name, PublicKey FROM users WHERE Active = 1 ORDER BY Name ASC";
		PreparedStatement ps = this.dbconn.prepareStatement(sql);
		
		ResultSet rs = ps.executeQuery();
		
		if (!rs.first()) {
			return retval;
		}
		
		do {
			retval.add(rs.getString("Id") + ":" + rs.getString("Name") + ":" + rs.getString("PublicKey"));
		} while(rs.next());
		
		return retval;
	}

	public int getOwnerID(int assetid) throws SQLException, RowNotFoundException {
		int ownerid = 0;
		String sql = "SELECT OwnerID FROM assets WHERE Id = ?";
		PreparedStatement ps = this.dbconn.prepareStatement(sql);
		ps.setInt(1, assetid);
		
		ResultSet rs = ps.executeQuery();
		if (!rs.first()) {
			throw new RowNotFoundException("Query for assetid " + assetid + " returned no results.");
		}
		ownerid = rs.getInt("OwnerID");
		return ownerid;
	}

	public boolean verifyUserID(int userid) throws SQLException {
		String sql = "SELECT Id FROM users WHERE Id = ?";
		PreparedStatement ps = this.dbconn.prepareStatement(sql);
		ps.setInt(1, userid);
		
		return ps.executeQuery().first();
	}

	public int addAsset(int oid, int uid, String payload) throws SQLException {
		String sql = "INSERT INTO assets (OwnerID, UserID, Payload) VALUES (?, ?, ?)";
		PreparedStatement ps = this.dbconn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		ps.setInt(1, uid);
		ps.setInt(2, uid);
		ps.setString(3, payload);
		ps.execute();

		ResultSet rs = ps.getGeneratedKeys();
		rs.first(); // selects returned row with generated primary key
		int assetid = rs.getInt(1);

		ps = this.dbconn.prepareStatement("UPDATE assets SET ParentId = ? WHERE Id = ?");
		ps.setInt(1, assetid);
		ps.setInt(2, assetid);
		ps.execute();

		return assetid;
	}

	public void updateAsset(int assetid, String payload) throws SQLException {
		String sql = "UPDATE assets SET Payload = ? WHERE Id = ?";
		PreparedStatement ps = this.dbconn.prepareStatement(sql);
		ps.setString(1, payload);
		ps.setInt(2, assetid);
		ps.execute();
	}

	public int shareAsset(int ownerid, int otheruserid, int parentassetid, String payload) throws SQLException {
		// note: this is just different enough from addAsset that we separate the two for cleanliness and security
		String sql = "INSERT INTO assets (OwnerId, UserId, ParentId, Payload) VALUES (?, ?, ?, ?)";
		PreparedStatement ps = this.dbconn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		ps.setInt(1, ownerid);
		ps.setInt(2, otheruserid);
		ps.setInt(3, parentassetid);
		ps.setString(4, payload);
		ps.execute();

		ResultSet rs = ps.getGeneratedKeys();
		rs.first(); // selects returned row with generated primary key

		return rs.getInt(1);
	}

	public void deleteAsset(int assetid) throws SQLException {
		// this deletes original and all shared copies
		String sql = "DELETE FROM assets WHERE Id = ?"; // this will delete children because of cascaded deletion
		PreparedStatement ps = this.dbconn.prepareStatement(sql);
		ps.setInt(1, assetid);
		ps.execute();
	}
	
	public void deleteAssetByParentAndUser(int parentid, int userid) throws SQLException {
		// this deletes original and all shared copies
		String sql = "DELETE FROM assets WHERE ParentId = ? and UserId = ?";
		PreparedStatement ps = this.dbconn.prepareStatement(sql);
		ps.setInt(1, parentid);
		ps.setInt(2, userid);
		ps.execute();
	}
}
