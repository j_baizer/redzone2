package com.redzone.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

import com.redzone.shared.AssetException;
import com.redzone.shared.AuthenticationException;
import com.redzone.shared.ConnectionException;
import com.redzone.shared.InvalidArgumentException;
import com.redzone.shared.InvalidPayloadException;
import com.redzone.shared.InvalidSessionException;
import com.redzone.shared.NonceException;
import com.redzone.shared.Packet;
import com.redzone.shared.RowNotFoundException;
import com.redzone.shared.UserAccountException;

public class Server implements Runnable {
	public static final String[] TLS_ALLOWED_CIPHERS = { "TLS_DHE_DSS_WITH_AES_256_GCM_SHA384" }; // list allowed cipher suites here.
	public static final String[] TLS_ALLOWED_PROTOCOLS = { "TLSv1.2" }; // list allowed SSL/TLS protocols here.

	public static final int NONCE_SIZE = 32;
	
	static final String DB_CONNECTION_URL = "jdbc:mysql://localhost/redzone?" + "user=redzone&password=2pass4redzone";
	static final int LOGLEVEL = Logger.DEBUG;
	private Socket sock;
	private Database rzdb = null;
	private String sessid;
	private int userid = 0;
	private String nonce;
	private boolean authenticated = false;

	private Server(Socket s) {
		this.sock = s;
	}

	public static void main(String[] args) { // main daemon loop
		Database rzdb = new Database(Server.DB_CONNECTION_URL);

		SSLServerSocket ss = null;
		try {
			ss = Server.openConnection(rzdb);
		} catch (ConnectionException e) {
			throw new RuntimeException(e);
		}

		rzdb.log.write("RZServer listening on TCP/TLS port " + ServerConfig.SERVER_PORT, Logger.NOTE);

		try {
			while (true) {
				new Thread(new Server(ss.accept())).start();
			}
		} catch (IOException e) {
			rzdb.log.write("Exception caught while accepting a new client: " + e.getClass().getName() + ": " + e.getMessage(), Logger.FATAL);
		}

	}

	public void run() { // for individual client connections connection threads need to make their own db and log connections however we can put them in the class members for convenience
		this.rzdb = new Database(Server.DB_CONNECTION_URL);

		rzdb.log = new Logger(Server.LOGLEVEL, rzdb.getConnection());
		rzdb.log.write("New Client Connection Thread Started", Logger.NOTE);

		// parse xml packet perform traffic control on client requests
		PrintWriter out = null;
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			out = new PrintWriter(this.sock.getOutputStream(), true);
			while (true) {
				String xml = in.readLine();
				if (xml == null || xml.isEmpty()) {
					throw new InvalidArgumentException("Invalid XML rejected");
				}

				Packet rzp = new Packet(xml, rzdb.getLog());
				if (!rzp.isValid()) {
					throw new InvalidArgumentException("Invalid XML rejected");
				}
				rzdb.log.write("XML: " + rzp.encodeToXML(), Logger.DEBUG);

				// at this point, we have a syntactically valid and parsed packet
				ArrayList<String> args = rzp.getArgv();
				if (args.isEmpty()) {
					throw new InvalidPayloadException("No request specified in args, closing connection");
				}

				String req = args.get(0); // arg[0] is the operation requested
				switch (req) {
					case ("init"):
						this.handleInit(out, rzp);
						break;
					case ("auth"):
						this.handleAuth(out, rzp);
						break;
					case ("add"):
						this.handleAdd(out, rzp);
						break;
					case ("update"):
						this.handleUpdate(out, rzp);
						break;
					case ("share"):
						this.handleShare(out, rzp);
						break;
					case ("delete"):
						this.handleDelete(out, rzp);
						break;
					case ("alist"):
						this.handleAssetList(out, rzp);
						break;
					case ("ulist"):
						this.handleUserList(out, rzp);
						break;
					default:
						throw new InvalidArgumentException("Unsupported request received (" + req + ") closing connection!");
				}
			}
		} catch (IOException e) { // this happens when a CLIENT drops the connection, so it's only a debug message
			rzdb.log.write("Connection dropped", Logger.DEBUG);
		} catch (Exception e) {
			rzdb.log.write("Error in client thread: " + e.getClass().getName() + ": " + e.getMessage(), Logger.DEBUG);
		} finally {
			try {
				closeConnection(out);
			} catch (Exception e) {
				rzdb.log.write("Exception caught while closing connection: " + e.getClass().getName() + ": " + e.getMessage(), Logger.ERROR);
			}
		}

	}

	private static SSLServerSocket openConnection(Database rzdb) throws ConnectionException {
		SSLServerSocket ssock = null;
		try {
			// ***STEPS FOR CREATING SELF-SIGNED TLS CERTS:
			// Use Java keytool to create a key store with a private key for the
			// server 'key store'
			// Use keytool to export a self-signed cert for that key
			// Import that cert into a NEW key store for the client's use as a
			// 'trust store'
			// Both files get placed in the root folder of the respective
			// application (above /src and /bin)
			System.setProperty("javax.net.ssl.keyStore", ServerConfig.KEYSTORE_PATH);
			System.setProperty("javax.net.ssl.keyStorePassword", ServerConfig.KEYSTORE_PASSWORD);

			SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
			ServerSocket s = ssf.createServerSocket(ServerConfig.SERVER_PORT);
			ssock = (SSLServerSocket) s; // we only use this to get/set SSL settings

			ssock.setEnabledCipherSuites(TLS_ALLOWED_CIPHERS); // comment this out to get full list
			System.out.println("Cipher Suites Enabled on Server:");
			for (String cipher : ssock.getEnabledCipherSuites()) {
				System.out.println("\t" + cipher);
			}

			ssock.setEnabledProtocols(TLS_ALLOWED_PROTOCOLS);
			System.out.println("SSL/TLS Protocols Enabled on Server:");
			for (String protocol : ssock.getEnabledProtocols()) {
				System.out.println("\t" + protocol);
			}

			return ssock;
		} catch (IOException e) {
			throw new ConnectionException(e);
		}
	}

	private void closeConnection(PrintWriter out) throws ConnectionException {
		this.authenticated = false;
		if (this.sock.isConnected() && out != null) {
			try {
				//out.println(""); // sending a newline makes life easier on the client side
				this.sock.close();
			} catch (IOException e) {
				throw new ConnectionException(e);
			}
		}

	}

	/**********************
	 * REQUEST HANDLERS *
	 **********************/
	private void handleInit(PrintWriter out, Packet rzp) {
		this.authenticated = false; // this *should* never be true here-playing it safe

		this.nonce = Server.randomBase64Nonce();

		rzp.setRandomSessionID();
		this.sessid = rzp.getSessionID(); // this is the ONLY time we write this
		rzp.setPayload(this.nonce);
		rzdb.log.write("init packet received", Logger.DEBUG, rzp);
		out.println(rzp.encodeToXML());

	}

	// handles authenticating user
	private void handleAuth(PrintWriter out, Packet rzp) throws InvalidArgumentException, InvalidSessionException {
		this.authenticated = false; // *should* always be false here-playing it safe
		rzdb.log.write("auth packet received", Logger.DEBUG, rzp);

		if (rzp.getArgv().size() != 3) { // all we should see here is auth, name, and pass
			throw new InvalidArgumentException("Incorrect number of arguments (got " + rzp.getArgv().size() + ", expecting 3)");
		}

		if (!rzp.getSessionID().equals(this.sessid)) {
			throw new InvalidSessionException("Bad SessionID (got " + rzp.getSessionID() + ", expected " + this.sessid);
		}

		if (this.nonce.trim().length() == 0) {
			throw new NonceException("Foiled attempt to re-use nonce");
		}

		String user = rzp.getArgv().get(1).trim();
		if (user.length() < Packet.MIN_NAME_LENGTH) {
			throw new InvalidArgumentException("SHORT username received");
		}
		if (user.length() > Packet.MAX_NAME_LENGTH) {
			throw new InvalidArgumentException("LONG username received");
		}

		String clHash = rzp.getArgv().get(2).trim();
		if (clHash.length() != Packet.DIGEST_LENGTH) {
			throw new InvalidArgumentException("INVALID pwhash received");
		}

		String dbHash;
		try {
			dbHash = rzdb.getPassHash(user, this.nonce);
		} catch (NoSuchAlgorithmException | SQLException | RowNotFoundException e) {
			throw new AuthenticationException("Exception while hashing user's stored password", e);
		}

		this.nonce = ""; // prevent re-use

		// compare the received hash with the hash of the stored password...
		if (!clHash.equals(dbHash)) {
			throw new InvalidArgumentException("Authentication failed for user '" + user + "'");
		}
		
		// we're authenticated-build the response packet...
		Packet reply = new Packet();
		reply.setSessionID(rzp.getSessionID()); // carry forward sessionid
		reply.setRequestID(rzp.getRequestID()); // retain client's rid
		ArrayList<String> args = null;
		try {
			args = rzdb.getMetaData(user);
		} catch (SQLException | RowNotFoundException e) {
			throw new AuthenticationException("Unable to retrieve metadata for user '" + user + "'", e);
		}
		this.userid = Integer.parseInt(args.get(0));

		ArrayList<String> assets = null;
		try {
			assets = rzdb.getAssets(this.userid);
		} catch (SQLException e) {
			throw new AssetException("Unable to retrieve assets for user '" + user + "'", e);
		}
		if (assets == null) {
			throw new AssetException("Unable to retrieve assets for user '" + user + "'");
		}
		args.addAll(assets);

		reply.setArgv(args);
		rzdb.log.write("User " + user + " (id " + Integer.toString(this.userid) + ") authenticated", Logger.NOTE, rzp);
		out.println(reply.encodeToXML());
		System.out.println(reply.encodeToXML());

		// if down here we have successfully authenticated and sent a response
		this.authenticated = true;

	}

	// handles inserting all NEW assets into the DB
	private void handleAdd(PrintWriter out, Packet rzp) throws InvalidPayloadException, InvalidArgumentException, InvalidSessionException {
		// note: this is just different enough from handleShare() that we separate the two for cleanliness and security
		rzdb.log.write("add packet received ", Logger.DEBUG, rzp);

		if (!this.authenticated) {
			throw new AuthenticationException("Cannot handleAdd for unauthenticated request");
		}

		if (rzp.getArgv().size() != 2) { // all we should see is add and a payload
			throw new InvalidArgumentException("Incorrect number of arguments (got " + rzp.getArgv().size() + ", expecting 2)");
		}

		if (!rzp.getSessionID().equals(this.sessid)) {
			throw new InvalidSessionException("Bad SessionID (got " + rzp.getSessionID() + ", expected " + this.sessid);
		}

		String payload = rzp.getArgv().get(1);
		if (payload.trim().isEmpty()) {
			throw new InvalidPayloadException("Invalid payload (blank)");
		}

		int assetid = 0;
		try {
			assetid = rzdb.addAsset(this.userid, this.userid, payload);
		} catch (SQLException e) {
			throw new AssetException("Unable to add asset for user '" + this.userid + "'", e);
		}

		// build a response packet
		Packet reply = new Packet();
		reply.setSessionID(rzp.getSessionID());
		reply.setRequestID(rzp.getRequestID());
		reply.setPayload(Integer.toString(assetid));
		out.println(reply.encodeToXML());

	}

	// handles updating/overwriting existing assets in the DB
	private void handleUpdate(PrintWriter out, Packet rzp) throws InvalidArgumentException, InvalidSessionException, InvalidPayloadException {
		rzdb.log.write("update packet received ", Logger.DEBUG, rzp);

		if (!this.authenticated) {
			throw new AuthenticationException("Cannot handleUpdate for unauthenticated reques");
		}

		if (rzp.getArgv().size() != 3) { // all we should see is update, assetid, and payload
			throw new InvalidArgumentException("Incorrect number of arguments (got " + rzp.getArgv().size() + ", expecting 3)");
		}

		if (!rzp.getSessionID().equals(this.sessid)) {
			throw new InvalidSessionException("Bad SessionID (got " + rzp.getSessionID() + ", expected " + this.sessid);
		}

		int assetid = Integer.parseInt(rzp.getArgv().get(1));
		if (assetid <= 0) {
			throw new AssetException("Invalid asset id (" + assetid + ")");
		}

		String payload = rzp.getArgv().get(2);
		if (payload.trim().isEmpty()) {
			throw new InvalidPayloadException("Invalid (blank) payload");
		}

		int ownerid;
		try {
			ownerid = rzdb.getOwnerID(assetid);
		} catch (SQLException | IllegalArgumentException | RowNotFoundException e) {
			throw new AssetException("Exception while handling assetid " + assetid, e);
		}

		if (ownerid != this.userid) {
			throw new AuthenticationException("Non-owner blocked from overwriting assetid " + assetid);
		}

		try {
			rzdb.updateAsset(assetid, payload);
		} catch (SQLException e) {
			throw new AssetException("Unable to update asset with id '" + assetid + "'");
		}

		// if we made it here, it was successful...build a response packet
		Packet reply = new Packet();
		reply.setSessionID(rzp.getSessionID());
		reply.setRequestID(rzp.getRequestID());
		reply.setPayload(Integer.toString(assetid));
		out.println(reply.encodeToXML());

	}

	// handles sharing of assets between users
	private void handleShare(PrintWriter out, Packet rzp) throws InvalidArgumentException, InvalidSessionException {
		// note: this is just different enough from handleAdd() that we separate the two for cleanliness and security
		rzdb.log.write("share packet received ", Logger.DEBUG, rzp);

		if (!this.authenticated) {
			throw new AuthenticationException("Cannot handleShare for unauthenticated reques");
		}

		// sanity-check the args
		if (rzp.getArgv().size() != 4) { // all we should see is share, assetid, otheruserid, and payload
			throw new InvalidArgumentException("Incorrect number of arguments (got " + rzp.getArgv().size() + ", expecting 4)");
		}

		if (!rzp.getSessionID().equals(this.sessid)) {
			throw new InvalidSessionException("Bad SessionID (got " + rzp.getSessionID() + ", expected " + this.sessid);
		}

		int parentassetid = Integer.parseInt(rzp.getArgv().get(1));
		if (parentassetid <= 0) {
			throw new AssetException("Invalid assetid (" + parentassetid + ")");
		}

		int ownerid;
		try {
			ownerid = rzdb.getOwnerID(parentassetid);
		} catch (SQLException | IllegalArgumentException | RowNotFoundException e) {
			throw new AssetException("Exception while handling parent assetid " + parentassetid, e);
		}

		if (ownerid != this.userid) {
			throw new AuthenticationException(
					"Non-owner " + this.userid + " blocked from sharing asset " + parentassetid);
		}

		int otheruserid = Integer.parseInt(rzp.getArgv().get(2));
		if (otheruserid <= 0) {
			throw new AuthenticationException("Invalid otheruserid (" + otheruserid + ")");
		}
		if (this.userid == otheruserid) {
			throw new AssetException("User " + this.userid + " attempted to share asset " + parentassetid + " with self");
		}
		try {
			if (!rzdb.verifyUserID(otheruserid)){
				throw new AssetException("User ID '" + otheruserid + "' is invalid.");
			}
		} catch (SQLException e) {
			throw new AssetException("Unable to verify user ID for user '" + otheruserid + "'");
		}

		String payload = rzp.getArgv().get(3);
		if (payload.trim().length() == 0) {
			throw new AssetException("User " + this.userid + " attempted to share empty asset payload with invalid user " + otheruserid);
		}

		try {
			rzdb.deleteAssetByParentAndUser(parentassetid, otheruserid);
		} catch (SQLException e) {
			throw new AssetException("Error when deleting existing shared asset", e);
		}
		
		int newassetid;
		try {
			newassetid = rzdb.shareAsset(this.userid, otheruserid, parentassetid, payload);
		} catch (SQLException e) {
			throw new AssetException("Unable to share asset", e);
		}
		if (newassetid <= 0) {
			throw new AssetException("New asset id is invalid");
		}

		// build a response packet
		Packet reply = new Packet();
		reply.setSessionID(rzp.getSessionID());
		reply.setRequestID(rzp.getRequestID());
		reply.setPayload(Integer.toString(newassetid));
		out.println(reply.encodeToXML());

	}

	// handles removal of assets by owners
	private void handleDelete(PrintWriter out, Packet rzp) throws InvalidArgumentException, InvalidSessionException {
		rzdb.log.write("delete packet received", Logger.DEBUG, rzp);

		if (!this.authenticated) {
			throw new AuthenticationException("Cannot handleDelete for unauthenticated request");
		}

		if (rzp.getArgv().size() != 2) { // all we should see is delete and assetid
			throw new InvalidArgumentException("Incorrect number of arguments (got " + rzp.getArgv().size() + ", expecting 2)");
		}

		if (!rzp.getSessionID().equals(this.sessid)) {
			throw new InvalidSessionException("Bad SessionID (got " + rzp.getSessionID() + ", expected " + this.sessid);
		}

		int assetid = Integer.parseInt(rzp.getArgv().get(1));
		if (assetid <= 0) { // this catches bad values in packet
			throw new AssetException("Invalid assetid " + assetid);
		}

		int ownerid;
		try {
			ownerid = rzdb.getOwnerID(assetid);
		} catch (SQLException | IllegalArgumentException | RowNotFoundException e) {
			throw new AssetException("Exception while handling assetid " + assetid, e);
		}

		if (this.userid != ownerid) {
			throw new AuthenticationException("Non-owner blocked from deleting asset " + assetid);
		}

		rzdb.log.write("User " + this.userid + " deleting asset " + assetid + " and all shared copies", Logger.NOTE, rzp);
		try {
			rzdb.deleteAsset(assetid);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new AssetException("Unable to delete asset with id " + assetid, e);
		}

		// we're all good-build and send a response packet
		Packet reply = new Packet();
		reply.setSessionID(rzp.getSessionID());
		reply.setRequestID(rzp.getRequestID());
		reply.setPayload(Integer.toString(1));
		out.println(reply.encodeToXML());

	}

	// handles request for asset list
	private void handleAssetList(PrintWriter out, Packet rzp) throws InvalidArgumentException, InvalidSessionException {
		rzdb.log.write("alist packet received", Logger.DEBUG, rzp);

		if (!this.authenticated) {
			throw new AuthenticationException("Cannot handleAssetList for unauthenticated reques");
		}

		if (rzp.getArgv().size() != 1) { // all we should see is alist
			throw new InvalidArgumentException("Incorrect number of arguments (got " + rzp.getArgv().size() + ", expecting 1)");
		}

		if (!rzp.getSessionID().equals(this.sessid)) {
			throw new InvalidSessionException("Bad SessionID (got " + rzp.getSessionID() + ", expected " + this.sessid);
		}

		ArrayList<String> assets = null;
		try {
			assets = rzdb.getAssets(this.userid);
		} catch (SQLException e) {
			throw new AssetException("Unable to retrieve asset list for userid " + this.userid, e);
		}
		if (assets == null) {
			throw new AssetException("Unable to retrieve asset list for userid " + this.userid);
		}

		// we're all good-build and send a response packet
		Packet reply = new Packet();
		reply.setSessionID(rzp.getSessionID());
		reply.setRequestID(rzp.getRequestID());
		reply.setArgv(assets);
		out.println(reply.encodeToXML());

	}

	// handles request for user list
	private void handleUserList(PrintWriter out, Packet rzp) throws InvalidArgumentException, InvalidSessionException, UserAccountException {
		rzdb.log.write("ulist packet received", Logger.DEBUG, rzp);

		if (!this.authenticated) {
			throw new AuthenticationException("Cannot handleUserList for unauthenticated request");
		}

		if (rzp.getArgv().size() != 1) { // all we should see is ulist
			throw new InvalidArgumentException("Incorrect number of arguments (got " + rzp.getArgv().size() + ", expecting 1)");
		}
		
		if (!rzp.getSessionID().equals(this.sessid)) {
			throw new InvalidSessionException("Bad SessionID (got " + rzp.getSessionID() + ", expected " + this.sessid);
		}

		ArrayList<String> users = null;
		try {
			users = rzdb.getUsers();
		} catch (SQLException e) {
			throw new AssetException("Unable to retrieve userlist", e);
		}
		if (users == null) {
			throw new UserAccountException("Unable to retrieve userlist");
		}

		// we're all good-build and send a response packet
		Packet reply = new Packet();
		reply.setSessionID(rzp.getSessionID());
		reply.setRequestID(rzp.getRequestID());
		reply.setArgv(users);
		out.println(reply.encodeToXML());
	}

	public static String randomBase64Nonce() {
		try {
			byte[] retval = new byte[NONCE_SIZE];
			SecureRandom.getInstanceStrong().nextBytes(retval);
			return Base64.getEncoder().encodeToString(retval);
		} catch (NoSuchAlgorithmException e) {
			throw new NonceException("Unable to Generate Nonce", e);
		}
	}
}
