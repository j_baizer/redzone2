package com.redzone.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.redzone.shared.Packet;

public class Logger {
	public static final int DEBUG = 0;
	public static final int NOTE = 1;
	public static final int WARN = 2;
	public static final int ERROR = 3;
	public static final int FATAL = 4;
	private int loglevel;
	private Connection dbconn;

	public Logger(int level, Connection dbconn) {
		this.loglevel = level;
		this.dbconn = dbconn;
	}

	public void write(String msg, int level) {
		this.write(msg, level, null);
	}

	public void write(String msg, int level, Packet rzp) {
		String sid = "";
		String rid = "";
		if (rzp != null) {
			sid = rzp.getSessionID();
			rid = rzp.getRequestID();
		}

		if (this.loglevel <= Logger.DEBUG) {
			System.out.println(msg);
		}

		if (level >= this.loglevel) {
			try {
				PreparedStatement ps = dbconn.prepareStatement("INSERT INTO log (sessionid, requestid, entry, level) VALUES (?, ?, ?, ?)");
				ps.setString(1, sid);
				ps.setString(2, rid);
				ps.setString(3, msg);
				ps.setInt(4, level);
				ps.execute();
			} catch (SQLException e) {
				System.err.println("***UNABLE TO LOG TO DATABASE!!!!");
				System.err.println("Entry: " + msg);
				System.exit(1);
			}

			if (level >= Logger.FATAL) {
				System.exit(1);
			}
		}
	}

}
