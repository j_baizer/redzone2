package com.redzone.server;

public final class ServerConfig {
	public static final String KEYSTORE_PATH = "ssl/serverstore.jks";
	public static final String KEYSTORE_PASSWORD = "pass4redzone";
	public static final int SERVER_PORT = 3411;

	private ServerConfig() {
	}
}
