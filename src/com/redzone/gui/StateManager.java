package com.redzone.gui;

import java.awt.CardLayout;
import java.awt.event.WindowEvent;
import java.security.PrivateKey;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.redzone.client.AssetManager;
import com.redzone.client.ClientConfig;
import com.redzone.client.ClientConnection;
import com.redzone.shared.Asset;
import com.redzone.shared.ConnectionException;

public class StateManager extends JPanel {
	private static final long serialVersionUID = 3815085607279513067L;

	protected JFrame window;

	protected final String PASSWORD_STATE = "passwordState";
	protected final String MAIN_SCREEN_STATE = "mainScreenState";

	private Asset open;
	private ArrayList<Asset> visible = new ArrayList<>();

	private ClientConnection rzcc;
	private AssetManager rzam;

	private PasswordState passwordState;
	private MainScreenState mainScreenState;

	static CardLayout cl;

	private boolean successfullyDecrypted = false;

	PlainMessageDialog plainMessageDialog;
	ConfirmDialog confirmDialog;

	boolean isSuccessfullyDecrypted() {
		return successfullyDecrypted;
	}

	void setSuccessfullyDecrypted(boolean successfullyDecrypted) {
		this.successfullyDecrypted = successfullyDecrypted;
	}

	void showPlainMessage(String message) {
		plainMessageDialog.setMessage(message);
		plainMessageDialog.open();
	}

	public void connect() {
		try {
			rzcc = new ClientConnection(ClientConfig.SERVER_ADDRESS, ClientConfig.SERVER_PORT);
			setRzam(new AssetManager(rzcc));
		} catch (ConnectionException e) {
			showPlainMessage("An error occurred while connecting");
			window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
		}
	}

	/**
	 * @param window
	 */
	protected StateManager(final JFrame window) {
		this.window = window;

		// Initialize cards
		// cards = new JPanel(new CardLayout());
		setLayout(new CardLayout());
		cl = (CardLayout) getLayout();

		populateStates();

		window.getContentPane().add(this);
		cl.show(this, PASSWORD_STATE);

		plainMessageDialog = new PlainMessageDialog(this, UIConstants.DEFAULT_PANEL_COLOR_LIGHT,
				UIConstants.DEFAULT_PANEL_COLOR_DARK, 450, 200, "");
		confirmDialog = new ConfirmDialog(this, UIConstants.DEFAULT_PANEL_COLOR_LIGHT,
				UIConstants.DEFAULT_PANEL_COLOR_DARK, 450, 220, null);

		connect();

		MiscUtils.setIcon(window, UIConstants.LOGO_PATH + UIConstants.ICON_NAME);

		init();
	}

	/**
	 * adds all states
	 */
	private void populateStates() {
		passwordState = new PasswordState(this);
		mainScreenState = new MainScreenState(this);

		add(mainScreenState, MAIN_SCREEN_STATE);
		add(passwordState, PASSWORD_STATE);
	}

	protected void setState(String stateName) {
		cl.show(this, stateName);
	}

	protected void init() {
		mainScreenState.init();
		passwordState.init();
	}

	protected void update() {
		mainScreenState.update();
		passwordState.update();
	}

	public JFrame getWindow() {
		return window;
	}

	public ClientConnection getRzcc() {
		return rzcc;
	}

	public void setRzcc(ClientConnection rzcc) {
		this.rzcc = rzcc;
	}

	public AssetManager getRzam() {
		return rzam;
	}

	public void setRzam(AssetManager rzam) {
		this.rzam = rzam;
	}

	public Asset getOpen() {
		return open;
	}

	public void setOpen(Asset open) {
		this.open = open;
	}

	public ArrayList<Asset> getVisible() {
		return visible;
	}

	public void setVisible(ArrayList<Asset> visible) {
		this.visible = visible;
	}

	public void setPrivateKey(PrivateKey pk) {
		rzam.setPrivateKey(pk);
	}

	public void logout() {
		if (isSuccessfullyDecrypted()) {
			getRzam().clean();
		}
		window.setTitle(English.PROGRAM_NAME);
		setState(PASSWORD_STATE);
		init();
	}
}
