package com.redzone.gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.redzone.shared.AssetException;
import com.redzone.shared.ConnectionException;
import com.redzone.shared.Asset;
import com.redzone.shared.DecryptedAsset;
import com.redzone.shared.InvalidArgumentException;
import com.redzone.shared.InvalidPayloadException;

public class AddRecordFieldDialog extends CustomDialog {
	private static final long serialVersionUID = 8371635546036175500L;

	private static final int SOUTH_PANEL_LAYOUT_HGAP = 10;

	private JLabel title;

	private JTextField textField;

	private CustomButton cancelButton;
	private CustomButton addRecordButton;

	public AddRecordFieldDialog(final StateManager sm, Color c1, Color c2, int w, int h) {
		super(sm, c1, c2, w, h);
		// --------------------north panel----------------------------------
		northPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		title = new JLabel(English.ADD_A_RECORD_FOLDER);
		title.setFont(new Font(UIConstants.FONT_STYLE, Font.PLAIN, UIConstants.DIALOGUE_BOX_TITLE_FONT_SIZE));
		northPanel.add(title);

		// --------------------Center panel----------------------------------
		centerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		textField = new JTextField();
		textField.setPreferredSize(UIConstants.DIALOG_TEXT_FIELD_DIMENSION);
		centerPanel.add(textField);

		// --------------------South panel----------------------------------
		southPanel.setLayout(new FlowLayout(FlowLayout.CENTER, SOUTH_PANEL_LAYOUT_HGAP, 0));

		cancelButton = setupButton(English.CANCEL, UIConstants.DIALOG_BOX_BUTTON_WIDTH,
				UIConstants.DIALOG_BOX_BUTTON_HEIGHT);
		cancelButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				close();
			}
		});

		addRecordButton = setupButton(English.ADD_RECORD, UIConstants.DIALOG_BOX_BUTTON_WIDTH,
				UIConstants.DIALOG_BOX_BUTTON_HEIGHT);
		addRecordButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (isValidName(textField.getText())) {
					addRecord();
					close();
				} else {
					sm.showPlainMessage(English.NOT_A_VALID_NAME);
				}
			}
		});

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (textField.getText().isEmpty()) {
					textField.setText("");
				}
			}
		});

		southPanel.add(cancelButton);
		southPanel.add(addRecordButton);
	}

	private void addRecord() {
		try {
			Asset newAsset = sm.getRzam().add(new DecryptedAsset(textField.getText().toCharArray(), new char[0]));
			sm.getVisible().add(newAsset);
		} catch (AssetException | ConnectionException | InvalidPayloadException | InvalidArgumentException e) {
			sm.showPlainMessage(e.getMessage());
			sm.logout();
		}
	}

	private boolean isValidName(String text) {
		return text != null && !text.trim().isEmpty();
	}

	@Override
	protected void init() {
		textField.setText("");
	}
}
