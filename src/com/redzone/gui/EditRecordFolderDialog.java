package com.redzone.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.redzone.shared.AssetException;
import com.redzone.shared.ConnectionException;
import com.redzone.shared.CryptoException;
import com.redzone.shared.InvalidArgumentException;
import com.redzone.shared.InvalidPayloadException;
import com.redzone.shared.Asset;

public class EditRecordFolderDialog extends CustomDialog {
	private static final long serialVersionUID = -3569511422230983023L;

	private static String name;
	private int index;

	private JLabel titleLabel;
	private JLabel nameLabel;
	private JTextField textField;
	private CustomButton cancelButton;
	private CustomButton deleteButton;
	private CustomButton renameButton;

	private static final int NAME_FONT_SIZE = 20;
	private static final int CENTER_PANEL_TOP = 20;
	private static final int CENTER_PANEL_BOTTOM = 20;
	private static final int SOUTH_PANEL_HGAP = 10;

	public EditRecordFolderDialog(final StateManager sm, Color c1, Color c2, int w, int h, int i) {
		super(sm, c1, c2, w, h);
		// --------------------north panel----------------------------------
		index = i;
		name = getName(index, sm);
		titleLabel = new JLabel(English.EDIT);
		nameLabel = new JLabel(name);
		northPanel.setLayout(new BorderLayout());
		titleLabel.setFont(new Font(UIConstants.FONT_STYLE, Font.BOLD, UIConstants.DIALOGUE_BOX_TITLE_FONT_SIZE));
		nameLabel.setFont(new Font(UIConstants.FONT_STYLE, Font.ITALIC, NAME_FONT_SIZE));
		northPanel.add(titleLabel, BorderLayout.WEST);
		northPanel.add(nameLabel, BorderLayout.CENTER);

		// --------------------Center panel----------------------------------
		centerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		textField = new JTextField(name);
		centerPanel.setBorder(new EmptyBorder(CENTER_PANEL_TOP, 0, CENTER_PANEL_BOTTOM, 0));
		textField.setPreferredSize(UIConstants.DIALOG_TEXT_FIELD_DIMENSION);
		centerPanel.add(textField);

		// --------------------South panel----------------------------------
		southPanel.setLayout(new FlowLayout(FlowLayout.CENTER, SOUTH_PANEL_HGAP, 0));
		cancelButton = setupButton(English.CANCEL, UIConstants.DIALOG_BOX_BUTTON_WIDTH,
				UIConstants.DIALOG_BOX_BUTTON_HEIGHT);
		deleteButton = setupButton(English.DELETE, UIConstants.DIALOG_BOX_BUTTON_WIDTH,
				UIConstants.DIALOG_BOX_BUTTON_HEIGHT);
		renameButton = setupButton(English.RENAME, UIConstants.DIALOG_BOX_BUTTON_WIDTH,
				UIConstants.DIALOG_BOX_BUTTON_HEIGHT);
		southPanel.add(cancelButton);
		southPanel.add(deleteButton);
		southPanel.add(renameButton);

		// --------------------Listeners----------------------------------
		cancelButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				close();
			}
		});
		deleteButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				sm.confirmDialog.setMessage(English.DELETE_RF_MESSAGE);
				sm.confirmDialog.setButtonText(English.CANCEL, English.DELETE);
				sm.confirmDialog.open();
				if (sm.confirmDialog.getConfirmation()) {
					deleteFolder(sm, index);
					close();
				}
			}
		});
		renameButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (isValidText(textField.getText())) {
					renameFolder(textField.getText(), sm);
					sm.update();
					close();
				} else {
					sm.showPlainMessage(English.NOT_A_VALID_NAME);
				}
			}
		});
		textField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (textField.getText().equals(English.NEW_NAME)) {
					textField.setText("");
				}
			}
		});
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (isValidText(textField.getText())) {
					textField.setText(English.NEW_NAME);
				}
			}
		});
	}

	protected void renameFolder(String text, StateManager sm) {
		try {
			Asset asset = sm.getVisible().get(index);
			sm.getRzam().getDecrypted(asset).setName(textField.getText().toCharArray());
			sm.getRzam().update(asset);
			sm.update();
		} catch (AssetException | ConnectionException | InvalidPayloadException | InvalidArgumentException | CryptoException e) {
			sm.showPlainMessage(e.getMessage());
			sm.logout();
		}
	}

	protected void deleteFolder(StateManager sm, int i) {
		try {
			Asset asset = sm.getVisible().get(index);
			sm.getRzam().delete(asset);
			sm.getVisible().remove(asset);
			sm.setOpen(null);
			sm.update();
		} catch (AssetException | ConnectionException | InvalidPayloadException | InvalidArgumentException e) {
			sm.showPlainMessage(e.getMessage());
			sm.logout();
		}
	}

	private String getName(int i, StateManager sm) {
		Asset asset = sm.getVisible().get(index);
		return new String(sm.getRzam().getDecrypted(asset).getName());
	}

	private boolean isValidText(String text) {
		return text != null && !text.trim().isEmpty();
	}

	@Override
	protected void init() {
	}
}
