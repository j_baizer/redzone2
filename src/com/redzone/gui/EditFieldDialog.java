package com.redzone.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;

import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.redzone.shared.AssetException;
import com.redzone.shared.ConnectionException;
import com.redzone.shared.CryptoException;
import com.redzone.shared.InvalidArgumentException;
import com.redzone.shared.InvalidPayloadException;
import com.redzone.shared.Asset;

public class EditFieldDialog extends CustomDialog {
	private static final long serialVersionUID = -2200655084486626161L;
	private static String fieldContent;
	private static Asset asset;

	private JLabel titleLabel;
	private JPasswordField contentTextField;
	private CustomButton cancelButton;
	private CustomButton saveButton;

	public EditFieldDialog(final StateManager sm, Color c1, Color c2, int w, int h, int i) {
		super(sm, c1, c2, w, h);
		// --------------------north panel----------------------------------
		northPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		titleLabel = new JLabel(English.EDIT_FIELD);
		titleLabel.setFont(new Font(UIConstants.FONT_STYLE, Font.PLAIN, UIConstants.DIALOGUE_BOX_TITLE_FONT_SIZE));
		northPanel.add(titleLabel);

		// --------------------Center panel----------------------------------
		asset = sm.getOpen();

		fieldContent = new String(sm.getRzam().getDecrypted(asset).getValue());

		centerPanel.setLayout(new BorderLayout());

		contentTextField = new JPasswordField(fieldContent);
		contentTextField.setEchoChar((char) 0);

		centerPanel.setBorder(new EmptyBorder(UIConstants.CENTER_PANEL_TOP, 0, UIConstants.CENTER_PANEL_BOTTOM, 0));

		contentTextField.setPreferredSize(UIConstants.DIALOG_TEXT_FIELD_DIMENSION);

		centerPanel.add(contentTextField, BorderLayout.SOUTH);

		// --------------------South panel----------------------------------
		southPanel.setLayout(new FlowLayout(FlowLayout.CENTER, UIConstants.SOUTH_PANEL_HGAP, 0));
		cancelButton = setupButton(English.CANCEL, UIConstants.DIALOG_BOX_BUTTON_WIDTH,
				UIConstants.DIALOG_BOX_BUTTON_HEIGHT);
		saveButton = setupButton(English.SAVE, UIConstants.DIALOG_BOX_BUTTON_WIDTH,
				UIConstants.DIALOG_BOX_BUTTON_HEIGHT);
		southPanel.add(cancelButton);
		southPanel.add(saveButton);

		// --------------------Listeners----------------------------------
		cancelButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				close();
			}
		});

		saveButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				sm.getRzam().getDecrypted(asset).setValue(contentTextField.getPassword());
				try {
					sm.getRzam().update(asset);
				} catch (AssetException | ConnectionException | InvalidPayloadException
						| InvalidArgumentException | CryptoException ex) {
					sm.showPlainMessage(ex.getMessage());
					sm.logout();
					return;
				}
				sm.update();
				close();
			}
		});
	}

	@Override
	protected void init() {
		titleLabel.setFocusable(true);
		titleLabel.requestFocus();

	}

}
