package com.redzone.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import com.redzone.shared.AssetException;
import com.redzone.shared.ConnectionException;
import com.redzone.shared.CryptoException;
import com.redzone.shared.InvalidArgumentException;
import com.redzone.shared.InvalidPayloadException;
import com.redzone.shared.UserAccountException;

public class FolderDisplay extends BackgroundPanel {
	private static final long serialVersionUID = 4095435087994523978L;
	private Border border;

	private static int DISPLAY_WIDTH = 30;
	private static final int DISPLAY_HEIGHT = 500;

	private static final int BUTTON_WIDTH = 40;
	private static final int BUTTON_HEIGHT = 40;
	private static final int BORDER_WIDTH = 2;
	private static final int FONT_SIZE = 18;
	private static final int ADD_BUTTON_DIMENSION = 25;

	private static final int RECORD_FIELD_DIALOGUE_WIDTH = 450;
	private static final int RECORD_FIELD_DIALOGUE_HEIGHT = 200;
	private static final int MAX_LENGTH_ADD = 140;
	private static final int TEMP_LENGTH_ADD = 110;
	private static final int IPADY = 10;
	private static final int TOOLBAR_LAYOUT_HGAP = 5;
	private static final int UNIT_INCREMENT = 5;

	private JPanel toolBar;

	private JPanel fViewer;
	private JPanel folderPanel;
	private JScrollPane scrollPane;
	private JPanel bottomBar;
	private BackgroundPanel top;

	GridBagConstraints c = new GridBagConstraints();

	AddRecordFieldDialog addRecordField;

	StateManager sm;
	// RZAsset currentNode;
	protected JLabel directoryTitle;

	/**
	 * @param sm
	 *            the state is is being added to
	 */
	protected FolderDisplay(final StateManager sm) {
		super(MiscUtils.getBufferedGradImage(UIConstants.DEFAULT_PANEL_COLOR_LIGHT,
				UIConstants.DEFAULT_PANEL_COLOR_DARK, DISPLAY_WIDTH, sm.window.getHeight(), true));
		this.sm = sm;
		setLayout(new BorderLayout(0, 0));

		addRecordField = new AddRecordFieldDialog(sm, UIConstants.DEFAULT_PANEL_COLOR_LIGHT,
				UIConstants.DEFAULT_PANEL_COLOR_DARK, RECORD_FIELD_DIALOGUE_WIDTH, RECORD_FIELD_DIALOGUE_HEIGHT);

		border = BorderFactory.createMatteBorder(BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH,
				UIConstants.DEFAULT_PANEL_COLOR_BORDER);
		setBorder(border);
		setOpaque(true);

		top = new BackgroundPanel(MiscUtils.getBufferedGradImage(UIConstants.DEFAULT_PANEL_COLOR_LIGHT,
				UIConstants.DEFAULT_PANEL_COLOR_DARK, DISPLAY_WIDTH, sm.window.getHeight(), true));
		top.setTransparentAdd(true);
		top.setLayout(new GridBagLayout());
		top.setOpaque(false);
		top.setBorder(border);

		fViewer = new JPanel(new BorderLayout());
		folderPanel = new JPanel(new GridBagLayout());
		scrollPane = new JScrollPane(fViewer, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getVerticalScrollBar().setUnitIncrement(UNIT_INCREMENT);
		scrollPane.setBorder(null);
		bottomBar = new JPanel(new FlowLayout(FlowLayout.CENTER));

		toolBar = new JPanel(new BorderLayout(TOOLBAR_LAYOUT_HGAP, 0));
		toolBar.setOpaque(false);
		toolBar.setBorder(null);

		directoryTitle = new JLabel("Assets");
		directoryTitle.setHorizontalAlignment(SwingConstants.CENTER);
		directoryTitle.setFont(new Font(UIConstants.FONT_STYLE, Font.BOLD, FONT_SIZE));
		directoryTitle.setOpaque(false);

		CustomButton homeButton = setupToolBarButton(UIConstants.IMG_HOME);
		homeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					sm.getRzam().updateList();
					sm.setVisible(sm.getRzam().getAll());
					update();
				} catch (AssetException | ConnectionException | InvalidPayloadException | UserAccountException
						| InvalidArgumentException | CryptoException e1) {
					sm.showPlainMessage(e1.getMessage());
					sm.logout();
				}
			}
		});
		setTransparentAdd(true);
		toolBar.add(directoryTitle, BorderLayout.CENTER);
		toolBar.add(homeButton, BorderLayout.EAST);
		// -------------------CENTERBOX---------------------

		// -------------------BOTTOMBAR---------------------

		CustomButton addRecordOrField = new CustomButton(English.ADD_RECORD_FOLDER_TITLE, 0, 0, ADD_BUTTON_DIMENSION,
				ADD_BUTTON_DIMENSION);
		addRecordOrField.setImageFromFile(UIConstants.IMG_PLUS, true);
		addRecordOrField.setHorizontalAlignment(SwingConstants.LEFT);
		addRecordOrField.setHorizontalTextPosition(SwingConstants.RIGHT);

		addRecordOrField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addRecordField.open();

			}
		});

		fViewer.add(folderPanel, BorderLayout.NORTH);

		setTransparentAdd(true);
		bottomBar.add(addRecordOrField);
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = IPADY;
		c.gridx = 0;
		c.gridy = 0;
		top.add(toolBar, c);
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		c.ipady = IPADY;
		c.gridx = 0;
		c.gridy = 1;
		top.add(scrollPane, c);

		add(top, BorderLayout.CENTER);
		add(bottomBar, BorderLayout.SOUTH);
		// add(fViewer, BorderLayout.CENTER);
	}

	protected void init() {
		sm.setVisible(sm.getRzam().getAll());
	}

	/**
	 * updates which folder the display is on
	 */
	protected void update() {
		folderPanel.removeAll();

		directoryTitle.setText("Assets");
		resizeDisplay();
		if (sm.getVisible().size() > 0) {
			folderPanel.setVisible(true);
			for (int i = 0; i < sm.getVisible().size(); i++) {
				FolderDisplayButton fdb;
				c.gridy = i;
				c.anchor = GridBagConstraints.NORTHWEST;
				c.fill = GridBagConstraints.HORIZONTAL;

				fdb = new FolderDisplayButton(new String(sm.getRzam().getDecrypted(sm.getVisible().get(i)).getName()),
						0, 0, DISPLAY_WIDTH, BUTTON_HEIGHT, i, sm, FolderDisplayButton.RECORD);

				folderPanel.add(fdb, c);
				folderPanel.revalidate();
				folderPanel.repaint();
			}
		} else {
			folderPanel.setVisible(false);
		}
		fViewer.revalidate();
		fViewer.repaint();
	}

	private void resizeDisplay() {
		JLabel l = new JLabel();
		l.setFont(new Font(UIConstants.FONT_STYLE, Font.BOLD, FONT_SIZE));
		int maxLength = (int) (directoryTitle.getPreferredSize().getWidth() + MAX_LENGTH_ADD);

		l.setFont(new Font(UIConstants.FONT_STYLE, Font.PLAIN, BUTTON_HEIGHT / 2));
		int temp = 0;

		for (int i = 0; i < sm.getVisible().size(); i++) {
			l.setText(new String(sm.getRzam().getDecrypted(sm.getVisible().get(i)).getName()));
			temp = (int) (l.getPreferredSize().getWidth() + TEMP_LENGTH_ADD);
			if (temp > maxLength) {
				maxLength = temp;
			}
		}
		DISPLAY_WIDTH = maxLength;
		setImage(MiscUtils.getBufferedGradImage(UIConstants.DEFAULT_PANEL_COLOR_LIGHT,
				UIConstants.DEFAULT_PANEL_COLOR_DARK, DISPLAY_WIDTH, sm.window.getHeight(), true));
		setSize(new Dimension(DISPLAY_WIDTH, DISPLAY_HEIGHT));
		revalidate();
		repaint();
	}

	private CustomButton setupToolBarButton(String imgPath) {
		CustomButton b = new CustomButton("", 0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		b.setImageIcon(MiscUtils.layerBufferedImages(
						MiscUtils.getBufferedGradImage(UIConstants.BUTTON_COLOR_LIGHT, UIConstants.BUTTON_COLOR_DARK, BUTTON_WIDTH, BUTTON_HEIGHT, true),
						MiscUtils.getBufferedImageFromFile(imgPath, BUTTON_WIDTH)
						),
						true);
		return b;
	}
}
