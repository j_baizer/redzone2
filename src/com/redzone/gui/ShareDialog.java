package com.redzone.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import com.redzone.shared.AssetException;
import com.redzone.shared.ConnectionException;
import com.redzone.shared.CryptoException;
import com.redzone.shared.InvalidArgumentException;
import com.redzone.shared.InvalidPayloadException;
import com.redzone.shared.Asset;
import com.redzone.shared.User;
import com.redzone.shared.UserAccountException;

public class ShareDialog extends CustomDialog {
	private static final long serialVersionUID = -3569511422230983023L;

	private static String name;
	private int index;

	private JLabel titleLabel;
	private JLabel nameLabel;
	private JComboBox<User> people;
	private CustomButton cancelButton;
	private CustomButton doneButton;

	private static final int NAME_FONT_SIZE = 20;
	private static final int CENTER_PANEL_TOP = 20;
	private static final int CENTER_PANEL_BOTTOM = 20;
	private static final int SOUTH_PANEL_HGAP = 10;

	public ShareDialog(final StateManager sm, Color c1, Color c2, int w, int h, int i) {
		super(sm, c1, c2, w, h);
		// --------------------north panel----------------------------------
		index = i;
		name = getName(index, sm);
		titleLabel = new JLabel(English.EDIT);
		nameLabel = new JLabel(name);
		northPanel.setLayout(new BorderLayout());
		titleLabel.setFont(new Font(UIConstants.FONT_STYLE, Font.BOLD, UIConstants.DIALOGUE_BOX_TITLE_FONT_SIZE));
		nameLabel.setFont(new Font(UIConstants.FONT_STYLE, Font.ITALIC, NAME_FONT_SIZE));
		northPanel.add(titleLabel, BorderLayout.WEST);
		northPanel.add(nameLabel, BorderLayout.CENTER);

		// --------------------Center panel----------------------------------
		centerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		people = new JComboBox<>();
		centerPanel.setBorder(new EmptyBorder(CENTER_PANEL_TOP, 0, CENTER_PANEL_BOTTOM, 0));
		// textField.setPreferredSize(Consts.DIALOGUE_TEXT_FIELD_DIMENSION);
		centerPanel.add(people);

		// --------------------South panel----------------------------------
		southPanel.setLayout(new FlowLayout(FlowLayout.CENTER, SOUTH_PANEL_HGAP, 0));
		cancelButton = setupButton(English.CANCEL, UIConstants.DIALOG_BOX_BUTTON_WIDTH,
				UIConstants.DIALOG_BOX_BUTTON_HEIGHT);
		doneButton = setupButton(English.SHARE, UIConstants.DIALOG_BOX_BUTTON_WIDTH,
				UIConstants.DIALOG_BOX_BUTTON_HEIGHT);
		southPanel.add(cancelButton);
		southPanel.add(doneButton);

		// --------------------Listeners----------------------------------
		cancelButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				close();
			}
		});
		doneButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					Asset asset = sm.getOpen();
					sm.getRzam().share(asset, ((User) people.getSelectedItem()).getUserid());
					close();
				} catch (AssetException | ConnectionException | UserAccountException | InvalidPayloadException
						| InvalidArgumentException | CryptoException ex) {
					sm.showPlainMessage(ex.getMessage());
					sm.logout();
				}
			}
		});
	}

	private String getName(int i, StateManager sm) {
		return "Sharing Settings";
	}

	public JComboBox<User> getPeopleDropdown() {
		return people;
	}

	@Override
	protected void init() {

	}

}
