package com.redzone.gui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;

import com.redzone.shared.Asset;
import com.redzone.shared.ConnectionException;
import com.redzone.shared.InvalidArgumentException;
import com.redzone.shared.InvalidPayloadException;
import com.redzone.shared.User;
import com.redzone.shared.UserAccountException;

public class RecordDisplay extends BackgroundPanel {
	private static final long serialVersionUID = 1255356460279101185L;
	private static final int DISPLAY_WIDTH = 250;
	private Border border;
	private int boarderWidth = 3;

	private StateManager sm;

	private static final int FIELD_DIALOGUE_WIDTH = 450;
	private static final int FIELD_DIALOGUE_HEIGHT = 220;

	private GridBagConstraints c = new GridBagConstraints();
	private GridBagConstraints g = new GridBagConstraints();

	private JPanel fViewer;
	private JPanel fieldPanel;
	private JScrollPane scrollPane;

	private static final int RECORD_TOOL_BAR_HEIGHT = 40;
	private static final int UNIT_INCREMENT = 5;

	private RecordToolBar recordToolBar;
	private CustomButton shareButton;
	private Asset currentAsset;

	protected RecordDisplay(final StateManager sm) {
		super(MiscUtils.getBufferedGradImage(UIConstants.ORANGE_PANEL_COLOR_LIGHT, UIConstants.ORANGE_PANEL_COLOR_DARK,
				DISPLAY_WIDTH, sm.window.getHeight(), true));
		this.sm = sm;

		final ShareDialog b = new ShareDialog(sm, UIConstants.DEFAULT_PANEL_COLOR_LIGHT,
				UIConstants.DEFAULT_PANEL_COLOR_DARK, FIELD_DIALOGUE_WIDTH, FIELD_DIALOGUE_HEIGHT, 0);
		b.setVisible(false);

		border = BorderFactory.createMatteBorder(boarderWidth, boarderWidth, boarderWidth, boarderWidth,
				UIConstants.ORANGE_PANEL_COLOR_DARK);
		setBorder(border);
		setOpaque(true);

		setLayout(new GridBagLayout());

		// -------------------TOOLBAR--------------------
		recordToolBar = new RecordToolBar(sm, DISPLAY_WIDTH, RECORD_TOOL_BAR_HEIGHT);

		// -------------------CENTERBOX---------------------

		fViewer = new JPanel(new BorderLayout());
		fieldPanel = new JPanel(new GridBagLayout());
		scrollPane = new JScrollPane(fViewer, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getVerticalScrollBar().setUnitIncrement(UNIT_INCREMENT);

		// -------------------BOTTOMBAR---------------------

		shareButton = new CustomButton(English.SHARE_WITH, 0, 0, 100, 30);
		shareButton.setGradientBackground(UIConstants.BUTTON_COLOR_LIGHT, UIConstants.BUTTON_COLOR_DARK, true);
		shareButton.setBoarderDetails(UIConstants.BUTTON_COLOR_BORDER, 2);

		shareButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				b.getPeopleDropdown().removeAllItems();
				try {
					for (User u : sm.getRzam().getUserList()) {
						if (u.getUserid() != sm.getRzcc().getUserID() && !sm.getOpen().getSharedWith().contains(u.getUserid())){ // if the user is not yourself and the user is not someone who the asset is already shared with
							b.getPeopleDropdown().addItem(u);
						}
					}
				} catch (ConnectionException | UserAccountException | InvalidPayloadException
						| InvalidArgumentException ex) {
					sm.showPlainMessage(ex.getMessage());
					sm.logout();
				}
				b.open();
			}
		});

		fViewer.add(fieldPanel, BorderLayout.NORTH);
		fViewer.add(shareButton, BorderLayout.PAGE_END);

		setTransparentAdd(true);
		g.weightx = 1;
		g.fill = GridBagConstraints.HORIZONTAL;
		g.ipady = 10;
		g.gridx = 0;
		g.gridy = 0;
		add(recordToolBar, g);
		g.weightx = 1;
		g.weighty = 1;
		g.fill = GridBagConstraints.BOTH;
		g.gridx = 0;
		g.gridy = 1;
		add(scrollPane, g);

	}

	protected void init() {
		setVisible(false);
	}

	protected void update() {
		currentAsset = sm.getOpen();
		if (currentAsset != null) {
			shareButton.setVisible(currentAsset.getOwnerID() == sm.getRzcc().getUserID());

			setVisible(true);
			recordToolBar.update();
			fieldPanel.removeAll();
			fieldPanel.repaint();

			/// -------------------------------------------------------------

			// RZDecryptedAsset recordData =
			// sm.getRzam().getDecrypted(currentAsset);
			FieldBox fb;

			fb = new FieldBox(0, 0, 10, 60, 1, sm);
			fb.setAlignmentY(TOP_ALIGNMENT);
			c.weightx = 1;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.ipady = 10;
			c.gridx = 0;
			c.gridy = 1;
			fieldPanel.add(fb, c);
			fieldPanel.revalidate();
			fieldPanel.repaint();

			// -------------------------------------------------------------

			revalidate();
			repaint();

		} else {
			recordToolBar.update();
			fieldPanel.removeAll();
			fieldPanel.repaint();
			setVisible(false);
		}
	}

}
