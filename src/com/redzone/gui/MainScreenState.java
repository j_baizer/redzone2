package com.redzone.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;

public class MainScreenState extends BackgroundPanel {
	private static final long serialVersionUID = 1L;

	private JPanel mainPanel = new JPanel();
	private SearchBar searchBar;
	private FolderDisplay folderDisplay;
	private RecordDisplay recordDisplay;
	private StateManager sm;

	protected MainScreenState(final StateManager sm) {

		super(MiscUtils.getBufferedGradImage(UIConstants.ORANGE_PANEL_COLOR_LIGHT, UIConstants.ORANGE_PANEL_COLOR_DARK, sm.window.getWidth(), sm.window.getHeight(), true));
		this.sm = sm;
		setTransparentAdd(true);
		searchBar = new SearchBar(sm);
		folderDisplay = new FolderDisplay(sm);
		recordDisplay = new RecordDisplay(sm);

		mainPanel.setLayout(new BorderLayout(0, 0));
		mainPanel.add(folderDisplay, BorderLayout.WEST);
		mainPanel.add(recordDisplay, BorderLayout.CENTER);

		setLayout(new BorderLayout(0, 0));
		add(searchBar, BorderLayout.NORTH);
		add(mainPanel, BorderLayout.CENTER);

	}

	protected void init() {
		folderDisplay.init();
		recordDisplay.init();
		sm.setOpen(null);
	}

	protected void update() {

		if (sm.getOpen() != null) {
			if (recordDisplay != null) {
				remove(recordDisplay);
			}
			recordDisplay.update();
		}
		recordDisplay.update();
		folderDisplay.update();
	}

}
