package com.redzone.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import com.redzone.shared.AssetException;
import com.redzone.shared.ConnectionException;
import com.redzone.shared.CryptoException;
import com.redzone.shared.InvalidArgumentException;
import com.redzone.shared.InvalidPayloadException;
import com.redzone.shared.UserAccountException;

public class PasswordState extends BackgroundPanel {
	private static final long serialVersionUID = 1L;
	private static final int BUTTON_WIDTH = 270;
	private static final int BUTTON_HEIGHT = BUTTON_WIDTH / 7;
	private static final int PASSWORD_WIDTH = 330;
	private static final Dimension PASSWORD_FIELD_DIMENSION = new Dimension(PASSWORD_WIDTH, 40);
	private static final int STARTUP_WIDTH = 600;
	private static final int STARTUP_HEIGHT = 250;
	private static final int TITLE_LABEL_Y = 75;
	private static final int TITLE_LABEL_WIDTH = 100;
	private static final int TITLE_LABEL_HEIGHT = 50;
	private static final int LOGO_HEIGHT = 20;
	private static final int PASSWORD_FIELD_FONT_SIZE = 24;

	private static final String TITLE = "RedZone";
	private StateManager sm;

	private JPanel centerPanel;

	private final JPasswordField usernameField;
	private final JPasswordField passWordField;

	StartUpDialog startup;

	protected PasswordState(final StateManager sm) {
		super(MiscUtils.getBufferedGradImage(UIConstants.DEFAULT_PANEL_COLOR_LIGHT,
				UIConstants.DEFAULT_PANEL_COLOR_DARK, sm.window.getWidth(), sm.window.getHeight(), true));
		this.sm = sm;

		startup = new StartUpDialog(sm, UIConstants.DEFAULT_PANEL_COLOR_LIGHT, UIConstants.DEFAULT_PANEL_COLOR_DARK,
				STARTUP_WIDTH, STARTUP_HEIGHT);

		setLayout(new BorderLayout());
		centerPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.ipady = 10;

		setTransparentAdd(true);
		JLabel titleLabel = new JLabel(TITLE);
		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		titleLabel.setFont(new Font(UIConstants.FONT_STYLE, Font.BOLD, UIConstants.DIALOGUE_BOX_TITLE_FONT_SIZE));
		titleLabel.setBounds(sm.window.getWidth() / 2 - 50, TITLE_LABEL_Y, TITLE_LABEL_WIDTH, TITLE_LABEL_HEIGHT);
		centerPanel.add(titleLabel, c);

		c.gridy += 1;
		JLabel lblSafeboxLogo = new JLabel();
		int logoWidth = 160;
		lblSafeboxLogo.setSize(new Dimension(logoWidth, LOGO_HEIGHT));
		BufferedImage logo = MiscUtils.getBufferedImageFromFile(UIConstants.LOGO_PATH + UIConstants.BIG_LOGO_NAME,
				lblSafeboxLogo.getWidth());
		lblSafeboxLogo.setIcon(new ImageIcon(logo));
		lblSafeboxLogo.setSize(new Dimension(logo.getWidth(), logo.getHeight()));
		centerPanel.add(lblSafeboxLogo, c);

		c.gridy++;
		setTransparentAdd(false);
		usernameField = new JPasswordField();
		usernameField.setPreferredSize(PASSWORD_FIELD_DIMENSION);
		usernameField.setFont(new Font(UIConstants.FONT_STYLE, Font.PLAIN, PASSWORD_FIELD_FONT_SIZE));
		usernameField.setHorizontalAlignment(SwingConstants.CENTER);
		resetUsernameField(usernameField);
		usernameField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (isInitField(usernameField.getPassword(), English.USERNAME_TITLE)) {
					usernameField.setText("");
					usernameField.setEditable(true);
				}
			}
		});
		centerPanel.add(usernameField, c);

		c.gridy++;
		setTransparentAdd(false);
		passWordField = new JPasswordField();
		passWordField.setPreferredSize(PASSWORD_FIELD_DIMENSION);
		passWordField.setFont(new Font(UIConstants.FONT_STYLE, Font.PLAIN, PASSWORD_FIELD_FONT_SIZE));
		passWordField.setHorizontalAlignment(SwingConstants.CENTER);
		resetPasswordField(passWordField);
		passWordField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (isInitField(passWordField.getPassword(), English.PASSWORD_TITLE)) {
					passWordField.setText("");
					passWordField.setEchoChar(UIConstants.ECHO_CHAR);
					passWordField.setEditable(true);
				}
			}
		});
		passWordField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					validatePasswordAndMoveForward(passWordField);
				}
			}
		});
		centerPanel.add(passWordField, c);

		c.gridy++;
		setTransparentAdd(true);
		CustomButton enterSBButton = setupButton(English.ENTER_PROGRAM_TITLE);
		enterSBButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				validatePasswordAndMoveForward(passWordField);
				init();
			}

		});
		centerPanel.add(enterSBButton, c);

		add(centerPanel, BorderLayout.CENTER);
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (passWordField.getPassword().length == 0) {
					resetPasswordField(passWordField);
				}
			}
		});
	}

	private void validatePasswordAndMoveForward(JPasswordField passwordField) {
		try {
			try {
				sm.getRzcc().authenticate(new String(usernameField.getPassword()), passwordField.getPassword());
				sm.setPrivateKey(sm.getRzcc().getPVK(passwordField.getPassword()));
				sm.getRzam().updateList();
			} catch (AssetException | ConnectionException | InvalidPayloadException | InvalidArgumentException
					| UserAccountException | CryptoException e) {
				e.printStackTrace();
				if (e instanceof ConnectionException){
					sm.connect();
				}
				sm.showPlainMessage(e.getMessage());
				sm.logout();
				return;
			}

			sm.setState(sm.MAIN_SCREEN_STATE);
			sm.setSuccessfullyDecrypted(true);
			sm.init();
			sm.update();
			sm.getWindow().setTitle(English.PROGRAM_NAME + " - Signed in as " + new String(usernameField.getPassword()));
		} catch (RuntimeException e) {
			sm.connect();
			sm.showPlainMessage(English.INCORRECT_PASSWORD_MESSAGE);
			passwordField.setText("");
		}
	}

	protected void update() {

	}

	public void init() {
		resetPasswordField(passWordField);
	}

	private void resetPasswordField(JPasswordField p) {
		p.setHorizontalAlignment(SwingConstants.CENTER);
		p.setText(English.PASSWORD_TITLE);
		p.setEchoChar((char) 0);
		p.setEditable(false);
	}

	private void resetUsernameField(JPasswordField p) {
		p.setHorizontalAlignment(SwingConstants.CENTER);
		p.setText(English.USERNAME_TITLE);
		p.setEchoChar((char) 0);
		p.setEditable(false);
	}

	private CustomButton setupButton(String text) {
		CustomButton b = new CustomButton(text, 0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		b.setGradientBackground(UIConstants.BUTTON_COLOR_LIGHT, UIConstants.BUTTON_COLOR_DARK, true);
		return b;
	}

	private boolean isInitField(char[] cs, String defaultVal) {
		return Arrays.equals(cs, defaultVal.toCharArray());
	}
}
