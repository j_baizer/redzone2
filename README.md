# RedZone #

### A Secure Cloud-Based Password Manager with Sharing Features ###

RedZone2 provides secure password storage and sharing without persisting any data locally or storing plaintext data on the server. All assets are encrypted and signed by the client before they are sent to the server. As a result, the server has no knowledge of the contents of assets, nor does it ever possess the keys needed to decrypt them. Redzone also features comprehensive configurable server side logging, which creates a full audit trail.

### Significant changes between RedZone and RedZone2 ###
* Support for propagating shared asset updates
* Support for unlimited length assets (no longer limited by RSA)
* Lots of code cleanup
* GUI improvements

### Acknowledgments ###
* RedZone2 by Jacob Baizer is a fork of [RedZone](https://bitbucket.org/j_baizer/redzone) with various enhancements (detailed above). RedZone was written in Fall 2017 for CYBR200 (Secure Software Systems) by Jacob Baizer and Brian Geiger.
* RedZone's GUI is based off of [SafeBox](https://bitbucket.org/j_baizer/fatalerror) which was written in Spring 2016 for COMP55 (Application Development) by Evan Sano, Michael Davis, Dominic Lesaca, and Jacob Baizer 

### Quickstart ###
* Checkout repo
* Install / Configure MySQL on server
* Add redzone credentials (in RZServer.java) to MySQL
* Import schema.sql into MySQL to setup redzone DB
* Install JDK v8 or newer
 * Depending on your Java version, you may need to install the Unlimited Strength JCE
* Install MySQL JDBC connector (Connector/J)
* Compile / Run RZServer on server
* Compile / Run testclient
* Compile / Run GUI
* Default users in schema.sql are "Test 1" and "Test 2" with password of p@ssw0rd
* IMPORTANT: Generate new TLS cert prior to any production use!

### Open Issues  ###
* Enrollment
* Password change
* Admin user management

### Screenshots ###
![](https://i.imgur.com/n4uFD6N.png)

RedZone client login screen.

![](https://i.imgur.com/p0HI4m2.png)

Signed in as Test 1, assets are visible on the left side. Bank Account is selected, making the contents of the asset visible in the main panel.

![](https://i.imgur.com/evrueYW.png)

The sharing settings for Test 1's bank account being used to share the credentials with Test 2.

![](https://i.imgur.com/jrNKb2n.png)

Signed in as Test 2, the credentials sent by Test 1 are visible.
